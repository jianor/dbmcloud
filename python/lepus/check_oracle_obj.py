#!//bin/env python
#coding:utf-8
import os
import sys
import string
import time
import datetime
import MySQLdb
import cx_Oracle
import logging
import logging.config
logging.config.fileConfig("etc/logger.ini")
logger = logging.getLogger("lepus")
path='./include'
sys.path.insert(0,path)
import functions as func
import lib_oracle as oracle
import threading
import sendmail

db_type = 'oracle'
warning = 'warning'
critical = 'critical'
ok = 'ok'
connectivity = 'connectivity'
table_server = 'oracle_server'
table_status = 'oracle_status'
table_tbs = 'oracle_tablespace'
mail_receiver_list = func.get_config('mail_server','mail_receiver_list')


def check_oracle(host,port,dsn,username,password,tags):
    url=host+':'+port+'/'+dsn
    server = func.get_current_server(table_server,host,port)
    send_mail = server[8]
    send_mail_to_list = server[9]
    send_sms = server[10]
    send_sms_to_list = server[11]
    alarm_session_total = server[12]
    alarm_session_actives = server[13]
    alarm_session_waits = server[14]
    alarm_tablespace = server[15]
    threshold_session_total = server[16]
    threshold_session_actives = server[17]
    threshold_session_waits = server[18]
    threshold_tablespace = server[19]
    now_time = func.get_now_time()

    if send_mail_to_list is None or  send_mail_to_list.strip()=='':
       send_mail_to_list = mail_receiver_list

    try:
        conn=cx_Oracle.connect(username,password,url) #获取connection对象
        logger.info("check oracle %s : connect success." %(url))
    except Exception, e:
        error_msg = str(e).strip('\n')
        logger_msg="check oracle %s : %s" %(url,error_msg)
        logger.warning(logger_msg)
	func.add_alarm(db_type,tags,host,port,now_time,connectivity,'error',critical,'connect error',error_msg,send_mail,send_sms)
        if send_mail==1:
            func.send_alarm_mail(db_type,tags,host,port,now_time,critical,'connect error',error_msg,send_mail_to_list)
         
        try:
            connect=0
            func.arch_data_to_history(table_status,host,port)
            sql="insert into oracle_status(host,port,tags,connect) values(%s,%s,%s,%s)"
            param=(host,port,tags,connect)
            func.mysql_exec(sql,param)
        except Exception, e:
            logger.error(str(e).strip('\n'))
            sys.exit(1)
        finally:
            sys.exit(1)
    
    finally:
        func.check_db_status(host,port,tags,'oracle')   

    try:
       session_total = oracle.get_sessions(conn)
       session_total_count = int(len(session_total))
       if int(alarm_session_total) == 1 and ( int(session_total_count) >= int(threshold_session_total) ):
          message = "%s session total"%(session_total_count)
          session_total_table = oracle.format_sessions_table(session_total)
          func.add_alarm(db_type,tags,host,port,now_time,'session total',session_total_count,warning,message,session_total_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_total_table,send_mail_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)


    try:
       session_actives = oracle.get_actives(conn)
       session_actives_count = int(len(session_actives))
       if int(alarm_session_actives) == 1 and ( int(session_actives_count) >= int(threshold_session_actives) ):
          message = "%s session actives"%(session_actives_count)
          session_actives_table = oracle.format_sessions_table(session_actives)
          func.add_alarm(db_type,tags,host,port,now_time,'session actives',session_actives_count,warning,message,session_actives_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_actives_table,send_mail_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)


    try:
       session_waits = oracle.get_waits(conn)
       session_waits_count = int(len(session_waits))
       if int(alarm_session_waits) == 1 and ( int(session_waits_count) >= int(threshold_session_waits) ):
          message = "%s session waits"%(session_waits_count)
          session_waits_table = oracle.format_sessions_table(session_waits)
          func.add_alarm(db_type,tags,host,port,now_time,'session waits',session_waits_count,warning,message,session_waits_table,send_mail,send_sms)
          if send_mail == 1:
             func.send_alarm_mail(db_type,tags,host,port,now_time,warning,message,session_waits_table,send_mail_to_list)
    except Exception, e:
       logger.error(str(e).strip('\n'))
       sys.exit(1)

    try:
        #get info by v$instance
        connect = 1
        instance_name = oracle.get_instance(conn,'instance_name')
        instance_role = oracle.get_instance(conn,'instance_role')
        database_role = oracle.get_database(conn,'database_role')
        open_mode = oracle.get_database(conn,'open_mode')
        protection_mode = oracle.get_database(conn,'protection_mode')
        if database_role == 'PRIMARY':  
            database_role_new = 'm'  
            dg_stats = '-1'
            dg_delay = '-1'
        else:  
            database_role_new = 's'
            #dg_stats = oracle.get_stats(conn)
            #dg_delay = oracle.get_delay(conn)
            dg_stats = '1'
            dg_delay = '1'

        instance_status = oracle.get_instance(conn,'status')
        startup_time = oracle.get_instance(conn,'startup_time')
        #print startup_time
        #startup_time = time.strftime('%Y-%m-%d %H:%M:%S',startup_time) 
        #localtime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        #uptime =  (localtime - startup_time).seconds        
        #print uptime
        uptime = oracle.get_instance(conn,'startup_time')
        version = oracle.get_instance(conn,'version')
        instance_status = oracle.get_instance(conn,'status')
        database_status = oracle.get_instance(conn,'database_status')
        host_name = oracle.get_instance(conn,'host_name')
        archiver = oracle.get_instance(conn,'archiver')
        #get info by sql count
        session_total = session_total_count
        session_actives = session_actives_count
        session_waits = session_waits_count
        #get info by v$parameters
        parameters = oracle.get_parameters(conn)
        processes = parameters['processes']
        
        ##get info by v$parameters
        sysstat_0 = oracle.get_sysstat(conn)
        time.sleep(1)
        sysstat_1 = oracle.get_sysstat(conn)
        session_logical_reads_persecond = sysstat_1['session logical reads']-sysstat_0['session logical reads']
        physical_reads_persecond = sysstat_1['physical reads']-sysstat_0['physical reads']
        physical_writes_persecond = sysstat_1['physical writes']-sysstat_0['physical writes']
        physical_read_io_requests_persecond = sysstat_1['physical write total IO requests']-sysstat_0['physical write total IO requests']
        physical_write_io_requests_persecond = sysstat_1['physical read IO requests']-sysstat_0['physical read IO requests']
        db_block_changes_persecond = sysstat_1['db block changes']-sysstat_0['db block changes']
        os_cpu_wait_time = sysstat_0['OS CPU Qt wait time']
        logons_persecond = sysstat_1['logons cumulative']-sysstat_0['logons cumulative']
        logons_current = sysstat_0['logons current']
        opened_cursors_persecond = sysstat_1['opened cursors cumulative']-sysstat_0['opened cursors cumulative']
        opened_cursors_current = sysstat_0['opened cursors current']
        user_commits_persecond = sysstat_1['user commits']-sysstat_0['user commits']
        user_rollbacks_persecond = sysstat_1['user rollbacks']-sysstat_0['user rollbacks']
        user_calls_persecond = sysstat_1['user calls']-sysstat_0['user calls']
        db_block_gets_persecond = sysstat_1['db block gets']-sysstat_0['db block gets']

        #get current status value
        parse_time_cpu = sysstat_1['parse time cpu']
        parse_time_elapsed = sysstat_1['parse time elapsed']
        parse_count_total = sysstat_1['parse count (total)']
        parse_count_hard = sysstat_1['parse count (hard)']
        parse_count_failures = sysstat_1['parse count (failures)']
        
        #get last time status data
        last_data=func.mysql_query_one("select parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures from oracle_status where host='%s' and port='%s'" %(host,port))
        if last_data!=0:
           last_parse_time_cpu = last_data[0]
           last_parse_time_elapsed = last_data[1]
           last_parse_count_total = last_data[2]
           last_parse_count_hard = last_data[3]
           last_parse_count_failures = last_data[4]

           parse_time_cpu_inc = int(parse_time_cpu)-int(last_parse_time_cpu)
           parse_time_elapsed_inc = int(parse_time_elapsed)-int(last_parse_time_elapsed)
           parse_count_total_inc = int(parse_count_total)-int(last_parse_count_total)
           parse_count_hard_inc = int(parse_count_hard)-int(last_parse_count_hard)
           parse_count_failures_inc = int(parse_count_failures)-int(last_parse_count_failures)
        else:
           parse_time_cpu_inc = 0
           parse_time_elapsed_inc = 0
           parse_count_total_inc = 0
           parse_count_hard_inc = 0
           parse_count_failures_inc = 0
        

        #arch data to history
        func.arch_data_to_history(table_status,host,port)

        ##################### insert data to mysql server#############################
        sql = "insert into oracle_status(host,port,tags,connect,instance_name,instance_role,instance_status,database_role,open_mode,protection_mode,host_name,database_status,startup_time,uptime,version,archiver,session_total,session_actives,session_waits,dg_stats,dg_delay,processes,session_logical_reads_persecond,physical_reads_persecond,physical_writes_persecond,physical_read_io_requests_persecond,physical_write_io_requests_persecond,db_block_changes_persecond,os_cpu_wait_time,logons_persecond,logons_current,opened_cursors_persecond,opened_cursors_current,user_commits_persecond,user_rollbacks_persecond,user_calls_persecond,db_block_gets_persecond,parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures,parse_time_cpu_inc,parse_time_elapsed_inc,parse_count_total_inc,parse_count_hard_inc,parse_count_failures_inc) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);"
        param = (host,port,tags,connect,instance_name,instance_role,instance_status,database_role,open_mode,protection_mode,host_name,database_status,startup_time,uptime,version,archiver,session_total,session_actives,session_waits,dg_stats,dg_delay,processes,session_logical_reads_persecond,physical_reads_persecond,physical_writes_persecond,physical_read_io_requests_persecond,physical_write_io_requests_persecond,db_block_changes_persecond,os_cpu_wait_time,logons_persecond,logons_current,opened_cursors_persecond,opened_cursors_current,user_commits_persecond,user_rollbacks_persecond,user_calls_persecond,db_block_gets_persecond,parse_time_cpu,parse_time_elapsed,parse_count_total,parse_count_hard,parse_count_failures,parse_time_cpu_inc,parse_time_elapsed_inc,parse_count_total_inc,parse_count_hard_inc,parse_count_failures_inc)
        func.mysql_exec(sql,param) 
        func.update_db_status_init(database_role_new,version,host,port,tags)

        #check tablespace
        tablespace = oracle.get_tablespace(conn)
        if tablespace:
           #arch data to history
           func.arch_data_to_history(table_tbs,host,port)
           for line in tablespace:
              sql="insert into oracle_tablespace(host,port,tags,tablespace_name,total_size,used_size,avail_size,used_rate) values(%s,%s,%s,%s,%s,%s,%s,%s)"
              param=(host,port,tags,line[0],line[1],line[2],line[3],line[4])
              func.mysql_exec(sql,param)
           

    except Exception, e:
        logger.error(e)
        sys.exit(1)

    finally:
        conn.close()
        




def main():

    servers=func.mysql_query("select host,port,dsn,username,password,tags from oracle_server where monitor=1;")
    logger.info("check oracle controller start.")

    if servers:
       threads = []
       i=0
       for item in servers:
          thread=threading.Thread(target=check_oracle, args=(item[0],item[1],item[2],item[3],item[4],item[5]))
          thread.setName(i)
          threads.append(thread)
          i=i+1
       for thread in threads:
          i = int(thread.getName())
          threads[i].start()
          time.sleep(3)
       for thread in threads:
          threads[i].join()

    else:
        logger.warning("check oracle: not found any servers")

    logger.info("check oracle controller finished.")
                     


if __name__=='__main__':
    main()
