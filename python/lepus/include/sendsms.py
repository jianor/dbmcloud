#!/bin/env python
#-*-coding:utf-8-*-
import os
import sys
import time
import string
reload(sys) 
sys.setdefaultencoding('utf8')
import ConfigParser


def send_sms(sms_to_list,sms_msg):
    '''
    sms_to_list:发给谁
    sms_msg:短信内容
    '''
    '''
    sms_to_list_comma:多个短信接收者，用逗号拼接
    sms_to_list_semicolon:多个短信接收者，用分号拼接
    '''
    sms_to_list_comma = ",".join(sms_to_list)
    sms_to_list_semicolon = ";".join(sms_to_list)
    try:
        ######### you send sms code here ##############
        if(len(sms_to_list)>0):
          for phone in sms_to_list:
             print "Send sms to %s: %s" %(phone,sms_msg)
             
        ###############################################
        return True
    except Exception, e:
        print str(e)
        return False
