<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2016/2/19
 * Time: 13:19
 */

namespace common\helpers;

class MyHelper
{
    static function list_to_tree($list, $root = 0, $pk = 'id', $pid = 'parent_id', $child = '_child') {
        // 创建Tree
        $tree = array();
        if (is_array($list)) {
            // 创建基于主键的数组引用
            $refer = array();
            foreach ($list as $key => $data) {
                $refer[$data[$pk]] = &$list[$key];
            }
            foreach ($list as $key => $data) {
                // 判断是否存在parent
                $parentId = 0;
                if (isset($data[$pid])) {
                    $parentId = $data[$pid];
                }
                if ((string)$root == $parentId) {
                    $tree[] = &$list[$key];
                } else {
                    if (isset($refer[$parentId])) {
                        $parent = &$refer[$parentId];
                        $parent[$child][] = &$list[$key];
                    }
                }
            }
        }
        return $tree;
    }

    static function tree_to_list($tree, $level = 0, $pk = 'id', $pid = 'parent_id', $child = '_child') {
        $list = array();
        if (is_array($tree)) {
            foreach ($tree as $val) {
                $val['level'] = $level;
                if (isset($val['_child'])) {
                    $child = $val['_child'];
                    if (is_array($child)) {
                        unset($val['_child']);
                        $list[] = $val;
                        $list = array_merge($list, self::tree_to_list($child, $level + 1));
                    }
                } else {
                    $list[] = $val;
                }
            }
            return $list;
        }
    }


    static function popup_tree_menu($tree, $level = 0) {
        $level++;
        $html = "";
        if (is_array($tree)) {
            $html = "<ul class=\"tree_menu\">\r\n";
            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    $id = $val["id"];
                    if (empty($val["id"])) {
                        $id = $val["name"];
                    }
                    if (!empty($val["is_del"])) {
                        $del_class = "is_del";
                    } else {
                        $del_class = "";
                    }
                    if (isset($val['_child'])) {
                        $html = $html . "<li>\r\n<a class=\"$del_class\" node=\"$id\" ><i class=\"fa fa-angle-right level$level\"></i><span>$title</span></a>\r\n";
                        $html = $html . self::popup_tree_menu($val['_child'], $level);
                        $html = $html . "</li>\r\n";
                    } else {
                        $html = $html . "<li>\r\n<a class=\"$del_class\" node=\"$id\" ><i class=\"fa fa-angle-right level$level\"></i><span>$title</span></a>\r\n</li>\r\n";
                    }
                }
            }
            $html = $html . "</ul>\r\n";
        }
        return $html;
    }

    static function sub_tree_menu($tree, $level = 0) {
        $level++;
        $html = "";
        if (is_array($tree)) {
            $html = "<ul class=\"tree_menu\">\r\n";
            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    $id = $val["id"];
                    if (empty($val["id"])) {
                        $id = $val["name"];
                    }
                    if (isset($val['_child'])) {
                        $html = $html . "<li>\r\n<a node=\"$id\"><i class=\"fa fa-angle-right level$level\"></i><span>$title</span></a>\r\n";
                        $html = $html . self::sub_tree_menu($val['_child'], $level);
                        $html = $html . "</li>\r\n";
                    } else {
                        $html = $html . "<li>\r\n<a  node=\"$id\" ><i class=\"fa fa-angle-right level$level\"></i><span>$title</span></a>\r\n</li>\r\n";
                    }
                }
            }
            $html = $html . "</ul>\r\n";
        }
        return $html;
    }

    function dropdown_menu($tree, $level = 0) {
        $level++;
        $html = "";
        if (is_array($tree)) {
            foreach ($tree as $val) {
                if (isset($val["name"])) {
                    $title = $val["name"];
                    $id = $val["id"];
                    if (empty($val["id"])) {
                        $id = $val["name"];
                    }
                    if (isset($val['_child'])) {
                        $html = $html . "<li id=\"$id\" class=\"level$level\"><a>$title</a>\r\n";
                        $html = $html . self::dropdown_menu($val['_child'], $level);
                        $html = $html . "</li>\r\n";
                    } else {
                        $html = $html . "<li  id=\"$id\"  class=\"level$level\">\r\n<a>$title</a>\r\n</li>\r\n";
                    }
                }
            }
        }
        return $html;
    }

    static function rotate($a) {
        $b = array();
        if (is_array($a)) {
            foreach ($a as $val) {
                foreach ($val as $k => $v) {
                    $b[$k][] = $v;
                }
            }
        }
        return $b;
    }

}

