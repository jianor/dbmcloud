<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'storagePath' => '../storage/',
    'storageCachePath' => '../storage/cache/',

    'storageUrl' => 'http://storage.yihuike.com',

    //image file sub path.
    'goodsBrandLogoDir' => 'images/goods_brand_logo/',
    'SupplierCertPhotoDir' => 'images/supplier_cert_photo/',
    'cmsCoverDir' => 'images/cms_cover/',

    'UeditorImageUrlPrefix' => 'http://localhost/',
    'UeditorImagePathFormat' => 'yiicenter/storage/ueditor/images/{yyyy}-{mm}-{dd}/{time}{rand:6}',

    'icon-framework' => 'fa',  // Font Awesome Icon framework

];
