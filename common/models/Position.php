<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "position".
 *
 * @property integer $id
 * @property string $uuid
 * @property string $name
 * @property integer $sort_order
 * @property integer $organization_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Position extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort_order', 'organization_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['uuid'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'uuid' => Yii::t('app', 'Uuid'),
            'name' => Yii::t('app', 'Name'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'organization_id' => Yii::t('app', 'Organization ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Before save.
     *
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($insert) {
                $this->created_by = Yii::$app->user->id;
                $this->updated_by = Yii::$app->user->id;
                $this->organization_id = Yii::$app->user->identity->organization_id;
                $this->uuid = Yii::$app->uuid->uuid3(\Ramsey\Uuid\Uuid::NAMESPACE_DNS, Yii::$app->user->identity->organization_id.$this->name.time());
            }else{
                $this->updated_by = Yii::$app->user->id;
            }
            return true;
        }
        else
            return false;
    }

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

    public static function findTotal()
    {
        return static::find()->select(['id', 'name'])->where(['organization_id' => Yii::$app->user->identity->organization_id, 'status' => 1])->orderBy('sort_order ASC')->asArray()->all();
    }

    public static function getName($id)
    {
        if ($id && is_numeric($id)) {
            $result = static::find()->select(['id', 'name'])->where(['id' => $id])->one();
            if (isset($result)) {
                return $result->name;
            } else {
                return Yii::t('app', 'Not Set');
            }
        }
        return Yii::t('app', 'Not Set');
    }

}
