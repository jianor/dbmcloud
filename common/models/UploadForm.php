<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 2015/8/30 0030
 * Time: 11:54
 */

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends Model
{
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['logo'], 'file'],
        ];
    }
}