<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "organization".
 *
 * @property integer $id
 * @property string $name
 * @property integer $phone
 * @property integer $fax
 * @property string $email
 * @property string $url
 * @property string $address
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $created_at
 */
class Organization extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['email'], 'email'],
            [['url'], 'url'],
            [['created_at','updated_at', 'updated_by'], 'integer'],
            [['name','phone','fax', 'email', 'url', 'address'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Org Name'),
            'phone' => Yii::t('app', 'Org Phone'),
            'fax' => Yii::t('app', 'Org Fax'),
            'email' => Yii::t('app', 'Org Email'),
            'url' => Yii::t('app', 'Org Url'),
            'address' => Yii::t('app', 'Org Address'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Before save.
     * 
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            $this->updated_by = Yii::$app->user->id;
            return true;
        }
        else
            return false;
    }

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

    public static function getInfo(){
            $result = static::find()->select('*')->where(['id' => Yii::$app->user->identity->organization_id ])->one();
            if(isset($result)){
                return $result;
            }
    }

}
