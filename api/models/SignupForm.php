<?php
namespace api\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends \yii\db\ActiveRecord
{
    public $realname;
    public $mobile;
    public $password;

    public static function tableName(){
         return 'crm_user';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['realname', 'filter', 'filter' => 'trim'],
            ['realname', 'required'],
            //['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['realname', 'string', 'min' => 2, 'max' => 10],

            ['mobile', 'filter', 'filter' => 'trim'],
            ['mobile', 'required'],
            [['mobile'], 'number'],
            [['mobile'], 'string', 'min'=>11,'max' => 11],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        return array_merge(
            $labels,
            [
                'mobile' => '手机号码',
                'password' => '登录密码',
                'realname' => '真实姓名',

            ]
        );
    }
}
