<?php
namespace LaneWeChat;

use LaneWeChat\Core\Wechat;

define('BASE_PATH',str_replace('\\','/',realpath(dirname(__FILE__).'/'))."/");
define('ROOT_PATH',BASE_PATH.'../../');



/**
 * 微信插件唯一入口文件.
 * @Created by Lane.
 * @Author: lane
 * @Mail lixuan868686@163.com
 * @Date: 14-1-10
 * @Time: 下午4:00
 * @Blog: Http://www.lanecn.com
 */
//引入配置文件
include_once ROOT_PATH.'vendor/sdk/wechat/config.php';
//引入自动载入函数
include_once ROOT_PATH.'vendor/sdk/wechat/autoloader.php';
//调用自动载入函数
AutoLoader::register();
//初始化微信类
$wechat = new WeChat(WECHAT_TOKEN, TRUE);



//首次使用需要注视掉下面这1行（26行），并打开最后一行（29行）
echo $wechat->run();
//首次使用需要打开下面这一行（29行），并且注释掉上面1行（26行）。本行用来验证URL
//$wechat->checkSignature();