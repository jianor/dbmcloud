<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use api\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<style type="text/css">
.web640{margin:0 auto; width:99%;max-width:640px; margin-top:10px;}
.web640 img{
max-width:100%; 
 width:expression(this.width > 240 ? 100%: true); 
.web640 table{max-width:100%;}
</style>
<body>
    <?php $this->beginBody() ?>

        <div class="container web640">
        <?= $content ?>
        </div>


    <footer class="footer">
        <div class="container">
       <center>&copy; Jinzi Asset <?= date('Y') ?></center>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
