<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2015/8/26
 * Time: 15:11
 */

return [
    'Operate' => '操作',
    'Create ' => '创建',
    'Create' => '创建',
    'Update ' => '更新',
    'Update' => '更新',
    'Delete' => '删除',
    'List' => '列表',
    'Parent ID' => '上级分类',
    'User ID' => '用户ID',
    'Title' => '名称',
    'Surname' => '别名',
    'Description' => '介绍',
    'Content' => '内容',
    'Seo Title' => 'SEO标题',
    'Seo Keywords' => 'SEO关键字',
    'Seo Description' => 'SEO描述',
    'Sort Order' => '排序',
    'Click' => '点击数',
    'Status' => '状态',
    'Created At' => '创建时间',
    'Updated At' => '更新时间',

    'Product Catalog' => '商品分类',
    'Product Catalogs' => '商品分类',

    'Product Brand' => '商品品牌',
    'Product Brands' => '商品品牌',


];