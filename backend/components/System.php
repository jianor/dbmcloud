<?php
/**
 * Created by PhpStorm.
 * User: ruzuojun
 * Date: 2016/2/22
 * Time: 14:13
 */

namespace backend\components;



use Yii;
use yii\base\Component;
use yii\base\UserException;
use c006\alerts\Alerts;
use backend\models\Menu;
use backend\models\ActionLog;

class System extends Component{

    public $organization_id;

    public $user_id;

    protected $model;


    /**
     * Initialize the component
     */
    public function init()
    {
        parent::init();
        $this->organization_id = Yii::$app->user->identity->organization_id;
        $this->user_id = Yii::$app->user->id;
    }

    public function getId($model,$uuid){
        $item = $model::find()->where(['uuid'=>$uuid,'organization_id'=>$this->organization_id])->one();
        return isset($item) ? $item->id : 0;
    }

    public function checkHasChild($childModel,$condition){
        $msg = Yii::t('app', 'In using,Please delete the associated content.');
        $count = $childModel::find()->where($condition)->count();
        //echo $count;exit;
        if($count > 0){
            throw new UserException(Yii::t('app', $msg));
        }
    }

    //获取子类ID，返回ID数组
    public function getSubId($model,$field,$_ids=[] ){
        //@初始化栏目数组
        $ids_array[] = $_ids;

        do
        {
            $ids_list = '';
            //$temp = $this->mysql->select('SELECT `id` FROM `pcb_article_category` WHERE `parentID` IN (' . $categoryID . ')');
            $subItem = $model::find()->select(['id'])->where([$field=>$_ids])->all();
            foreach ($subItem as $v)
            {
                $ids_array[] = $v['id'];
                $ids_list .= ',' . $v['id'];
            }

            $ids_list = substr($ids_list, 1, strlen($ids_list));
            $_ids = explode(',',$ids_list);
        }
        while (!empty($subItem));

        return $ids_array;

        //$ids_list = implode(',', $ids_array);
        //return $ids_list;

    }

    public function getSubUserIds(){
        return $this->getSubId(\common\models\User::className(),'leader',$this->user_id);
    }

    public function getRealIp()
    {
        $ip=false;
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
            for ($i = 0; $i < count($ips); $i++) {
                if (!eregi ("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

    public  function writeLog($message=null){
        $user_id = $this->user_id;
        //$action = Yii::$app->controller->id.'/'.Yii::$app->requestedAction->id;
        $action = Yii::$app->request->url;
        //echo $action;exit;
        //$ip = $_SERVER["REMOTE_ADDR"];
        $ip = Yii::$app->request->userIP;
        $model = new ActionLog();
        $model->user_id = $user_id;
        $model->ip = $ip;
        $model->action = $action;
        $model->message = empty($message)? Menu::getNameByRoute(explode('?',$action)[0]) : $message;
        $model->organization_id = $this->organization_id;
        if($model->validate()){
            $model->save();
        }
        else{
            print_r($model->errors);exit;
        }
    }


}