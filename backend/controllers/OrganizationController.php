<?php

namespace backend\controllers;

use Yii;
use common\models\Organization;
use yii\web\NotFoundHttpException;
use backend\models\ActionLog;
use c006\alerts\Alerts;

class OrganizationController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = Organization::find()->where(['id'=>Yii::$app->user->identity->organization_id])->one();

        if(Yii::$app->request->isPost)
        {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                ActionLog::log("更新单位信息");
                Alerts::setMessage('单位信息更新成功!');
                Alerts::setAlertType(Alerts::ALERT_SUCCESS);
            }
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

}
