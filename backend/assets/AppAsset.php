<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
	//public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
        'adminlte/css/font-awesome.min.css',
        'adminlte/css/ionicons.min.css',
        'adminlte/css/AdminLTE.min.css',
        'adminlte/css/skins/_all-skins.min.css',
        //'adminlte/js/plugins/iCheck/all.css',
		'css/site.css',
        'css/patch.css',
        'css/iconfont.css',
        'css/chart.css',
    ];
    public $js = [
        'adminlte/js/AdminLTE/app.min.js',
        'adminlte/js/AdminLTE/skin.js',
        //'adminlte/js/plugins/slimScroll/jquery.slimscroll.min.js',
        //'adminlte/js/plugins/iCheck/icheck.min.js',

		'js/backend.js',
        'js/application.js',
        'js/echarts.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
