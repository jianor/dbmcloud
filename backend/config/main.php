<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'defaultRoute' => '/sys/default/index',
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
        'core' => [
            'class' => 'backend\modules\core\Module',
        ],
        'sys' => [
            'class' => 'backend\modules\sys\Module',
        ],
        'mysql' => [
            'class' => 'backend\modules\mysql\Module',
        ],
        'oracle' => [
            'class' => 'backend\modules\oracle\Module',
        ],
        'mongodb' => [
            'class' => 'backend\modules\mongodb\Module',
        ],
        'redis' => [
            'class' => 'backend\modules\redis\Module',
        ],
        'hbase' => [
            'class' => 'backend\modules\hbase\Module',
        ],
        'os' => [
            'class' => 'backend\modules\os\Module',
        ],
        'config' => [
            'class' => 'backend\modules\config\Module',
        ],
        'alarm' => [
            'class' => 'backend\modules\alarm\Module',
        ],
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing' => true,
            'rules' => [
                '<alias:login|logout|signup|index>' => 'site/<alias>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

                '<controller:\w+>/<uuid:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<uuid:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

		'request' => [
          'parsers' => [ // 因为wechat模块中有使用angular.js  所以该设置是为正常解析angular提交post数据
              'application/json' => 'yii\web\JsonParser'
           ]
      	],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js' => ['/js/jquery.min.js'], // we'll take JQuery from CDN
                    'jsOptions' => [
                        'position' => \yii\web\View::POS_HEAD,
                    ],
                ],

            ],
        ],

        'settings' => [
            'class' => 'backend\components\Settings',
        ],
        'system' => [
            'class' => 'backend\components\System',
        ],

    ],
    'params' => $params,
];
