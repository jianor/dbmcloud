<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;
use common\models\Department;
use common\models\Position;
use kartik\select2\Select2;
//use dosamigos\multiselect\MultiSelect;

Yii::$app->view->registerJsFile('/js/layer/layer.js');

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
<?php $form = ActiveForm::begin(); ?>

<div class="col-lg-6">
    <div class="panel panel-default">
       <div class="panel-heading">
                <?= Yii::t('app', 'User'); ?>
       </div>
       <div class="panel-body">
            
          <div class="user-form">

              <?= $form->field($model, 'realname')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'username')->hiddenInput(['maxlength' => 255])->label(false) ?>

              <?= $form->field($model, 'mobile')->textInput(['maxlength' => 255]) ?>
      
          <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>
      
          <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>



              <?= $form->field($model, 'telphone')->textInput(['maxlength' => 255]) ?>




          </div>
       </div>
    </div>
 </div>

  
<div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Yii::t('app', 'Roles'); ?>
            </div>

            <div class="panel-body">

                <?= $form->field($model, 'department')->dropDownList(
                    ArrayHelper::merge(['' => Yii::t('app', 'Please Select')], ArrayHelper::map(Department::getTree(0, Department::getAll()), 'id', 'name'))
                ) ?>

                <?= $form->field($model, 'position')->dropDownList(
                    ArrayHelper::merge(['' => Yii::t('app', 'Please Select')], ArrayHelper::map(Position::findTotal(), 'id', 'name'))
                ) ?>
                <div  class="f-left">
                <?= $form->field($model, 'leader')->dropDownList([$model->leader=>\common\models\User::getRealname($model->leader)],['maxlength' => true,'readonly'=>true,'style'=>'width:160px' ]) ?>
            </div>
            <div  class="f-left r-select-button">
                <?= Html::buttonInput(Yii::t('app', 'Select'), ['class' => 'btn bg-red ', 'name' => 'select-user-button', 'id' => 'select-user']) ?>
                <input type="hidden" name="object_id" id="object_id" value="user-leader">
            </div>
            <div class="clear"></div>

                <?php
                /*
                echo MultiSelect::widget([
                'id'=>"multiXX",
                "options" => ['multiple'=>"multiple"], // for the actual multiselect
                'data' => [ 0 => 'super', 2 => 'natural'], // data as array
                'value' => [ 0, 2], // if preselected
                'name' => 'role', // name for the form
                "clientOptions" =>
                [
                "includeSelectAllOption" => true,
                'numberDisplayed' => 2
                ],
                ]);
                */
                ?>

                <div class="clear"></div>

                <label class="control-label" for="role"><?=Yii::t('app','Role ').Yii::t('app','Permission') ?></label>
                <?= Select2::widget([
                    //'data' => $model->getUnassignedItems(),
                    'data'=>ArrayHelper::map(User::getRole(),'name','description'),
                    'value'=> $model->explodeRole($model->role),
                    'language' => 'en',
                    'name' => 'User[role]',
                    'options' => ['placeholder' => '请分配角色权限...'],
                    'pluginOptions' => [
                        'id' => 'name',
                        //'allowClear' => true,
                        'multiple' => true,
                    ],
                ]);
                ?>

                 <?php
                 /*
                 echo $form->field($model, 'role')->widget(Select2::classname(), [
                    //'data' => $model->getUnassignedItems(),
                     'data'=>ArrayHelper::map(User::getRole(),'name','description'),
                     'language' => 'en',
                     'name' => 'role',
                     'options' => ['placeholder' => '请分配角色权限...'],
                     'pluginOptions' => [
                         'id' => 'name',
                         //'allowClear' => true,
                         'multiple' => true,
                     ],
                 ]);
                 */
                 ?>

                <?= $form->field($model, 'status')->dropDownList(User::getArrayStatus()) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
        </div>
</div>

<?php ActiveForm::end(); ?>

</div>
