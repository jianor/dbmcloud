<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use \common\models\Department;
use yii\helpers\ArrayHelper;
use forecho\jqtree\JQTree;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = '';

?>
<div class="container-fluid user-index f12">
    <div class="row">
        <div class="col-xs-4">



            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>
                        部门 &nbsp;<input type="radio" class="minimal" name="type" value="1" checked >&nbsp; 职位&nbsp;<input type="radio" class="minimal" name="type" value="1"> &nbsp;角色&nbsp; <input type="radio" name="type" class="minimal" value="1"> </th>
                </tr>
                </thead>
                <tbody>
                <?php if(count($department)>0): ?>

                    <?php foreach($department as $item){ ?>
                        <tr data-key="1">
                            <td><a href="<?= Yii::$app->urlManager->createUrl(['user/layer','UserSearch[department]'=>$item['id'] ]) ?>"><?= $item['name']; ?></a></td>
                        </tr>
                    <?php } ?>
                <?php else: ?>
                    <tr><td colspan="3"><?= Yii::t('app','You Are No Department Yet!') ?> <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Department'), ['create'], ['class' => 'btn btn-warning btn-flat btn-xs']) ?></td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="col-xs-8">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    /*[
                        'attribute' => 'username',
                        'headerOptions' => ['width'=>'100'],
                    ],*/
                    [
                        'attribute' => 'realname',
                        'headerOptions' => ['width'=>'80'],
                    ],
                    [
                        'attribute' => 'mobile',
                        'headerOptions' => ['width'=>'110'],
                    ],
                    [
                        'attribute' => 'telphone',
                        'headerOptions' => ['width'=>'100'],
                    ],
                    'email',
                    [
                        'label'=>Yii::t('app', 'Operate'),
                        'headerOptions' => ['width'=>'50'],
                        'format'=>'raw',
                        'value' => function($model) use($object_id ){
                            return
                                Html::a('<span class="glyphicon glyphicon-plus"></span>',"javascript:void(0)",
                                    ['title' => Yii::t('app','Select'),'onclick'=>"selected_user('$object_id',$model->id,'$model->realname')" ]);
                        }
                    ],

                ],
            ]); ?>
        </div>
    </div>
</div>



