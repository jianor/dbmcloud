<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = '系统温馨提示';
//$this->context->layout = 'error';
?>
<div class="site-error" style="padding:15px;">

    <div class="alert alert-info">
        <h2><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> <?= nl2br(Html::encode($message)) ?></h2>
        <p>
            如有疑问,请和系统管理员取得联系.谢谢.&nbsp;<a href="<?php echo Yii::$app->request->referrer; ?>">点此返回之前页面</a>
        </p>
    </div>


</div>
