<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> 温馨提示!</h4>
        权限菜单项目非专业开发人员请勿修改和删除.否则可能引起系统访问异常.
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Menu'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'Title') ?> </th>
            <th><?= Yii::t('app', 'Route') ?> </th>
            <th><?= Yii::t('app', 'Level') ?> </th>
            <th><?= Yii::t('app', 'Sort') ?> </th>
            <th><?= Yii::t('app', 'Operate') ?></th>

        </tr>
        </thead>
        <tbody>
        <?php foreach($dataProvider as $item){ ?>
            <tr data-key="1">
                <td><?= $item['name']; ?></td>
                <td><?= $item['route']; ?></td>
                <td><?= $item['level']; ?></td>
                <td><?= $item['sort']; ?></td>
                <td>
                    <!--a href="<?= \Yii::$app->getUrlManager()->createUrl(['menu/create','parent_id'=>$item['id']]); ?>" title="<?= Yii::t('app', 'Add Sub Catalog');?>" data-pjax="0"><span class="glyphicon glyphicon-plus-sign"></span></a-->
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['menu/view','id'=>$item['id']]); ?>"" title="<?= Yii::t('app', 'View');?>" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['menu/update','id'=>$item['id']]); ?>"" title="<?= Yii::t('app', 'Update');?>" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['menu/delete','id'=>$item['id']]); ?>"" title="<?= Yii::t('app', 'Delete');?>" data-confirm="<?= Yii::t('app', 'Are you sure you want to delete this item?');?>" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

</div>
