<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ActionLog */

$this->title = Yii::t('app', 'Create ') . Yii::t('app', 'Action Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Action Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-log-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
