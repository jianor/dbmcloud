<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Favorites */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Favorites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="favorites-view">

    <p>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::button(Yii::t('app', 'Print'), ['class' => 'btn btn-info','onclick'=>'preview()']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'uuid' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'uuid' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <!--startprint-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            ['label'=>Yii::t('app','Category'),'value'=>\common\models\FavoritesCategory::getName($model->category_id)],
            'name',
            'url:url',
            'description:ntext',
            'remark',
            'created_at:datetime',
            'updated_at:datetime',
            ['label'=>Yii::t('app','Created By'),'value'=>\backend\models\User::getRealname($model->created_by)],
            ['label'=>Yii::t('app','Updated By'),'value'=>\backend\models\User::getRealname($model->updated_by)],
        ],
    ]) ?>
    <!--endprint-->

</div>
