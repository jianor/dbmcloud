<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\helpers\Tools;
/* @var $this yii\web\View */
/* @var $model common\models\Department */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <p>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'uuid' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'uuid' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            ['label'=>Yii::t('app', 'Level'),'value'=>Tools::ConvertNumber($model->level).Yii::t('app','Level').Yii::t('app','Department '),],
            ['label'=>Yii::t('app', 'Parent ').Yii::t('app', 'Department '),'value'=>\common\models\Department::getName($model->parent_id),],
            'sort',
            'created_at:datetime',
            'updated_at:datetime',
            ['label'=>Yii::t('app','Created By'),'value'=>\backend\models\User::getUserame($model->created_by),],
            ['label'=>Yii::t('app','Updated By'),'value'=>\backend\models\User::getUserame($model->updated_by),],
        ],
    ]) ?>

</div>
