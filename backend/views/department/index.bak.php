<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\helpers\Tools;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Departments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Department'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <table class="table table-bordered">
        <colgroup>
            <col class="col-xs-2">
            <col class="col-xs-3">
            <col class="col-xs-3">
            <col class="col-xs-2">
            <col class="col-xs-2">
        </colgroup>
        <thead>
        <tr>
            <th>名称</th>
            <th>级别</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ((array)$dataList as $row):?>
            <tr>
                <th>
                    <?= $row['str_repeat'] ?> <?php echo $row['name']?>
                </th>
                <th><?= Tools::ConvertNumber($row['level']) ?>级部门</th>
                <td>
                    <a href="<?= Url::toRoute('department/update?id='.$row['id'] )?>" title="更新" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a> <a href="<?= Url::toRoute('department/delete?id='.$row['id'] )?>" title="删除" data-confirm="您确定要删除此项吗？" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>

</div>
