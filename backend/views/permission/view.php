<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use yii\widgets\DetailView;

$this->title = Yii::t('app', 'Permission') . " $model->name";
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('app', 'Permission'),
        'url' => ['/permission/index']
    ],
    $model->name
];
?>

<div class="permission-view">
   

        <?php
        echo DetailView::widget([
            'model' => $model,
            //'condensed' => true,
            //'hover' => true,
            //'mode' => DetailView::MODE_VIEW,
            //'enableEditMode' => false,
            /*'panel' => [
                'heading' => Icon::show('user') . Yii::t('auth', 'Role') .
                    Html::a(Icon::show('user') . Yii::t('auth', 'Update'), ['update', 'name' => $model->name], ['class' => 'btn-success btn-sm btn-dv pull-right']),
                //'type' => DetailView::TYPE_DEFAULT,
            ],*/
            'attributes' => [
                'name',
                'description'
           ],
        ]);
        ?>



</div>
