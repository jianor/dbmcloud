<?php
use backend\models\Module;
use backend\models\Menu;
use common\widgets\Menu as MenuWidget;



$module_unique_id = Yii::$app->controller->module->getUniqueId();
$module_id = Module::getId($module_unique_id);
//echo $module_id;
$parent = Menu::find()->where(['level'=>'1','parent_id'=>0,'module_id'=>$module_id])->one();
//var_dump($parent);
if($parent){
    $parent_id = $parent->id;
    $item_list = Menu::getMenuItems($parent_id, Menu::find()->where(['module_id'=>$module_id,'display'=>1 ])->orderBy('sort ASC,id ASC')->asArray()->all());

}
else{
    $item_list=[];
}
//echo $parent_id;

$menu_heaer = array(
  'label'=>'系统主菜单',
  'options'=>array('class' => 'header'),
);

echo MenuWidget::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => $item_list
    ]
);