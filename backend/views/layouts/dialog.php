<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title); ?></title>
    <?= Html::csrfMetaTags(); ?>
    <?php $this->head(); ?>
</head>
<body class="bg-white">
<div class="swiper-container">
    <div class="swiper-scrollbar"></div>
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="container popup">
                <?php $this->beginBody() ?>
                <?= $content ?>
                <?php $this->endBody() ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php $this->endPage() ?>

