<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\models\Menu;



$menuItems = Menu::getTopMenuList();

echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav '],
    'items' => $menuItems,
    'encodeLabels' => false,
]);


