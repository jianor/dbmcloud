<?php
use common\widgets\Menu;

echo Menu::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => [
            [
                'label' => '主导航菜单',
                'options' => [
                    'class' => 'header',
                ],
            ],
            [
                'label' => '我的首页（N）',
                'url' => Yii::$app->homeUrl,
                'icon' => 'fa-dashboard',
                'active' => Yii::$app->request->url === Yii::$app->homeUrl
            ],
            [
                'label' => '公告通知',
                'url' => ['#'],
                'icon' => 'fa fa-bell',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '最新公告',
                        'url' => ['/oa/notice/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '发布公告',
                        'url' => ['/oa/notice/create'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                ],
            ],
            [
                'label' => '日程管理',
                'url' => ['/oa/schedule/index'],
                'icon' => 'fa-table',
                'visible' => true,
            ],
            [
                'label' => '通讯录',
                'url' => ['/oa/contacts/index'],
                'icon' => 'fa fa-book',
                'visible' => true,
            ],
            [
                'label' => '收藏夹',
                'url' => ['/oa/favorites/index'],
                'icon' => 'fa fa-star',
                'visible' => true,
            ],
            /*
            [
                'label' => '系统设置',
                'url' => ['/setting/index'],
                'icon' => 'fa-cogs',
                'active' => Yii::$app->request->url === Yii::$app->request->baseUrl . '/setting/index',
                'visible' => Yii::$app->user->can('setting/index'),
            ],
            */

            [
                'label' => '商品管理（Y）',
                'url' => ['#'],
                'icon' => 'fa fa-suitcase',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '商品管理（Y）',
                        'url' => ['/goods/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '商品分类（Y）',
                        'url' => ['/goods-category/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '商品品牌（Y）',
                        'url' => ['/goods-brand/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                ],
            ],

            [
                'label' => '客户管理（Y）',
                'url' => ['#'],
                'icon' => 'fa fa-user-md',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '客户信息（Y）',
                        'url' => ['/customer/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '客户联系人（Y）',
                        'url' => ['/customer-person/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '客户纪念日（Y）',
                        'url' => ['/customer-memorial-day/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '客户服务单（N）',
                        'url' => ['/customer-service/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],


                ],
            ],

            [
                'label' => '机会管理',
                'url' => ['#'],
                'icon' => 'fa fa-shopping-cart',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '销售机会（Y）',
                        'url' => ['/sales-chance/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '机会跟踪（N）',
                        'url' => ['/sales-tracking/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '竞争分析（N）',
                        'url' => ['sales-compete/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '需求方案（N）',
                        'url' => ['sales-demand-plan/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '产品报价（N）',
                        'url' => ['#'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],

                ],
            ],

            [
                'label' => '合同管理',
                'url' => ['#'],
                'icon' => 'fa fa-book',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '业务合同（N）',
                        'url' => ['/procurement/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '交付计划（N）',
                        'url' => ['/procurement/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '交付记录（N）',
                        'url' => ['/procurement-detail/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                ],
            ],



            [
                'label' => '订单管理',
                'url' => ['#'],
                'icon' => 'fa fa-book',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '销售订单（N）',
                        'url' => ['/procurement/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '订单明细（N）',
                        'url' => ['/procurement-detail/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                ],
            ],







            [
                'label' => '供应商管理（Y）',
                'url' => ['#'],
                'icon' => 'fa fa-globe',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '供应商信息（Y）',
                        'url' => ['/supplier/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '供应商联系人（Y）',
                        'url' => ['/supplier-person/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '供应商证件照（Y）',
                        'url' => ['/supplier-cert/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '供应商报价管理（N）',
                        'url' => ['/supplier-quote/index'],
                        'icon' => 'fa fa-list-ul',
                        'visible' => Yii::$app->user->can('crm-earnings/index'),
                    ],
                ],
            ],

            /*
            [
                'label' => '采购管理',
                'url' => ['#'],
                'icon' => 'fa fa-truck',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '采购单',
                        'url' => ['/procurement/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '采购明细',
                        'url' => ['/procurement-detail/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                ],
            ],

            [
                'label' => '库存管理',
                'url' => ['#'],
                'icon' => 'fa fa-magnet',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '采购单',
                        'url' => ['/procurement/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '采购明细',
                        'url' => ['/procurement-detail/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                ],
            ],



            [
                'label' => '财务管理',
                'url' => ['#'],
                'icon' => 'fa fa-money',
                'visible' => Yii::$app->user->can('crm'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [

                    [
                        'label' => '资金注入抽取',
                        'url' => ['#'],
                        'icon' => 'fa fa-exchange',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '付款管理',
                        'url' => ['#'],
                        'icon' => 'fa fa-book',
                        'visible' => Yii::$app->user->can('crm-customer/group'),
                        'options' => [
                            'class' => 'treeview',
                        ],
                        'items' => [

                            [
                                'label' => '付款计划',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                            [
                                'label' => '付款记录',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                            [
                                'label' => '收票记录',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                        ]
                    ],
                    [
                        'label' => '收款管理',
                        'url' => ['#'],
                        'icon' => 'fa fa-book',
                        'visible' => Yii::$app->user->can('crm-customer/group'),
                        'options' => [
                            'class' => 'treeview',
                        ],
                        'items' => [

                            [
                                'label' => '收款计划',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                            [
                                'label' => '收款记录',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                            [
                                'label' => '开票记录',
                                'url' => ['#'],
                                'icon' => 'fa fa-list-alt',
                                'visible' => Yii::$app->user->can('crm-customer/my'),
                            ],
                        ]
                    ],
                ],
            ],
            */

            [
                'label' => '基础数据（Y）',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
                'visible' => true,
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '初始化数据',
                        'url' => ['/initialization/index'],
                        'icon' => 'fa fa-repeat',
                        'visible' => true,
                    ],
                    [
                        'label' => '行业分类',
                        'url' => ['/industry-type/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '经济类型',
                        'url' => ['/economic-type/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '销售方式',
                        'url' => ['/sales-method/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '客户等级',
                        'url' => ['/customer-level/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '客户来源',
                        'url' => ['/customer-source/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => Yii::$app->user->can('crm-customer/my'),
                    ],
                    [
                        'label' => '供应商等级',
                        'url' => ['/supplier-level/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '计量单位',
                        'url' => ['/measurement-unit/index'],
                        'icon' => 'fa fa-list-alt',
                        'visible' => true,
                    ],
                    [
                        'label' => '证照类型',
                        'url' => ['/cert-type/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],
                    [
                        'label' => '销售阶段',
                        'url' => ['/sales-stage/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],
                    [
                        'label' => '联系方式',
                        'url' => ['/contact-method/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],

                ],
            ],

            [
                'label' => '组织机构（Y）',
                'url' => ['#'],
                'icon' => 'fa fa-sitemap',
                'visible' => Yii::$app->user->can('organization'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    [
                        'label' => '单位管理',
                        'url' => ['/organization/index'],
                        'icon' => 'fa fa-user',
                        'visible' => Yii::$app->user->can('organization/index'),
                    ],
                    [
                        'label' => '部门管理',
                        'url' => ['/department/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],
                    [
                        'label' => '职位管理',
                        'url' => ['/position/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],
                    [
                        'label' => '角色权限',
                        'url' => ['/role/index'],
                        'icon' => 'fa fa-lock',
                        'visible' => Yii::$app->user->can('role/index'),
                    ],
                    [
                        'label' => '员工管理',
                        'url' => ['/user/index'],
                        'icon' => 'fa fa-user',
                        'visible' => true,
                    ],
                    [
                        'label' => '权限菜单',
                        'url' => ['/menu/index'],
                        'icon' => 'fa fa-lock',
                        'visible' => (Yii::$app->user->id == 1 && Yii::$app->user->identity->username == 'admin') ? true : false,
                    ],
                    [
                        'label' => '操作日志',
                        'url' => ['/action-log/index'],
                        'icon' => 'fa fa-lock',
                        'visible' => Yii::$app->user->can('action-log/index'),
                    ],
                ],
            ],



            /*
               [
                   'label' => '客户管理平台',
                   'url' => ['#'],
                   'icon' => 'fa fa-cog',
                   'visible' => Yii::$app->user->can('crm'),
                   'options' => [
                       'class' => 'treeview',
                   ],
                   'items' => [

                       [
                           'label' => '我录入的客户信息',
                           'url' => ['/crm-customer/my'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-customer/my'),
                       ],
                       [
                           'label' => '我的门店客户信息',
                           'url' => ['/crm-customer/group'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-customer/group'),
                       ],
                       [
                           'label' => '全部客户信息',
                           'url' => ['/crm-customer/index'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-customer/index'),
                       ],
                       [
                           'label' => '客户收益管理',
                           'url' => ['/crm-earnings/index'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-earnings/index'),
                       ],
                       [
                           'label' => '员工组管理',
                           'url' => ['/crm-employee-group/index'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-employee-group/index'),
                       ],
                       [
                           'label' => '员工管理',
                           'url' => ['/crm-employee/index'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-employee/index'),
                       ],
                       [
                           'label' => '创建报告',
                           'url' => ['/crm-report/create'],
                           'icon' => 'fa fa-user',
                           'visible' => Yii::$app->user->can('crm-report/create'),
                       ],


                   ],
               ],

               [
                   'label' => '商品管理',
                   'url' => ['#'],
                   'icon' => 'fa fa-cog',
                   'visible' => true,
                   'options' => [
                       'class' => 'treeview',
                   ],
                   'items' => [
                       [
                           'label' => '分类管理',
                           'url' => ['/product-catalog/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '品牌管理',
                           'url' => ['/product-brand/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '属性管理',
                           'url' => ['/product/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '商品管理',
                           'url' => ['/product/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '商品回收站',
                           'url' => ['/product/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],

                   ],
               ],

               [
                   'label' => '内容管理',
                   'url' => ['#'],
                   'icon' => 'fa fa-cog',
                   'visible' => true,
                   'options' => [
                       'class' => 'treeview',
                   ],
                   'items' => [
                       [
                           'label' => '分类管理',
                           'url' => ['/cms-catalog/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '文章管理',
                           'url' => ['/cms-article/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '页面管理',
                           'url' => ['/cms-page/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '案例管理',
                           'url' => ['/cms-cases/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '链接管理',
                           'url' => ['/cms-link/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],
                       [
                           'label' => '模型管理',
                           'url' => ['/cms-module/index'],
                           'icon' => 'fa fa-user',
                           'visible' => true,
                       ],

                   ],
               ],
           */
            /*
                [
                    'label' => 'Wiki管理',
                    'url' => ['#'],
                    'icon' => 'fa fa-cog',
                    'visible' => true,
                    'options' => [
                        'class' => 'treeview',
                    ],
                    'items' => [
                        [
                            'label' => 'Wiki管理',
                            'url' => ['/wiki/index'],
                            'icon' => 'fa fa-user',
                            'visible' => true,
                        ],
                        [
                            'label' => '分类管理',
                            'url' => ['/wiki-meta/index'],
                            'icon' => 'fa fa-user',
                            'visible' => true,
                        ],

                    ],
                ],
            */
            [
                'label' => '微信管理',
                'url' => ['#'],
                'icon' => 'fa fa-cog',
                'visible' => Yii::$app->user->can('wechat'),
                'options' => [
                    'class' => 'treeview',
                ],
                'items' => [
                    /*[
                        'label' => '微信公众号配置',
                        'url' => ['/wechat/index'],
                        'icon' => 'fa fa-user',
                        'visible' => Yii::$app->user->can('wechat/index'),
                    ],*/
                    [
                        'label' => '微信菜单管理',
                        'url' => ['/wechat-menu/index'],
                        'icon' => 'fa fa-user',
                        'visible' => Yii::$app->user->can('wechat-menu/index'),
                    ],
                    [
                        'label' => '微信H5页面管理',
                        'url' => ['/wechat-html/index'],
                        'icon' => 'fa fa-user',
                        'visible' => Yii::$app->user->can('wechat-html/index'),
                    ],
                    [
                        'label' => '微信注册用户',
                        'url' => ['/crm-user/index'],
                        'icon' => 'fa fa-user',
                        'visible' => Yii::$app->user->can('crm-user/index'),
                    ],

                ],

            ],

        ]
    ]
);