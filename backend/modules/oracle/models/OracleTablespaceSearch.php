<?php

namespace backend\modules\oracle\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\oracle\models\OracleTablespace;

/**
 * OracleTablespaceSearch represents the model behind the search form about `backend\modules\oracle\models\OracleTablespace`.
 */
class OracleTablespaceSearch extends OracleTablespace
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'total_size', 'used_size', 'avail_size'], 'integer'],
            [['host', 'port', 'tags', 'tablespace_name', 'used_rate', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OracleTablespace::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['used_rate' => SORT_DESC],
            ]
        ]);

        $dataProvider->sort->attributes['created_at'] = [
            'asc' => ['created_time' => SORT_ASC],
            'desc' => ['created_time' => SORT_DESC],
        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'total_size' => $this->total_size,
            'used_size' => $this->used_size,
            'avail_size' => $this->avail_size,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'tablespace_name', $this->tablespace_name])
            ->andFilterWhere(['like', 'used_rate', $this->used_rate]);

        return $dataProvider;
    }
}
