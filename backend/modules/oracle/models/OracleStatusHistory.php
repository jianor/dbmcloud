<?php

namespace backend\modules\oracle\models;

use Yii;

/**
 * This is the model class for table "oracle_status_history".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property integer $connect
 * @property string $instance_name
 * @property string $instance_role
 * @property string $instance_status
 * @property string $database_role
 * @property string $open_mode
 * @property string $protection_mode
 * @property string $host_name
 * @property string $database_status
 * @property string $startup_time
 * @property string $uptime
 * @property string $version
 * @property string $archiver
 * @property integer $session_total
 * @property integer $session_actives
 * @property integer $session_waits
 * @property string $dg_stats
 * @property integer $dg_delay
 * @property integer $processes
 * @property integer $session_logical_reads_persecond
 * @property integer $physical_reads_persecond
 * @property integer $physical_writes_persecond
 * @property integer $physical_read_io_requests_persecond
 * @property integer $physical_write_io_requests_persecond
 * @property integer $db_block_changes_persecond
 * @property integer $os_cpu_wait_time
 * @property integer $logons_persecond
 * @property integer $logons_current
 * @property integer $opened_cursors_persecond
 * @property integer $opened_cursors_current
 * @property integer $user_commits_persecond
 * @property integer $user_rollbacks_persecond
 * @property integer $user_calls_persecond
 * @property integer $db_block_gets_persecond
 * @property string $create_time
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $ymdhi
 */
class OracleStatusHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_status_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'port'], 'required'],
            [['connect', 'session_total', 'session_actives', 'session_waits', 'dg_delay', 'processes', 'session_logical_reads_persecond', 'physical_reads_persecond', 'physical_writes_persecond', 'physical_read_io_requests_persecond', 'physical_write_io_requests_persecond', 'db_block_changes_persecond', 'os_cpu_wait_time', 'logons_persecond', 'logons_current', 'opened_cursors_persecond', 'opened_cursors_current', 'user_commits_persecond', 'user_rollbacks_persecond', 'user_calls_persecond', 'db_block_gets_persecond', 'created_at', 'updated_at', 'ymdhi'], 'integer'],
            [['create_time'], 'safe'],
            [['host', 'instance_role', 'instance_status', 'database_role', 'host_name', 'version', 'archiver'], 'string', 'max' => 50],
            [['port'], 'string', 'max' => 10],
            [['tags', 'startup_time', 'uptime'], 'string', 'max' => 100],
            [['instance_name', 'open_mode', 'protection_mode', 'database_status'], 'string', 'max' => 30],
            [['dg_stats'], 'string', 'max' => 255],
            [['threshold_session_actives'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Host',
            'port' => 'Port',
            'tags' => 'Tags',
            'connect' => 'Connect',
            'instance_name' => 'Instance Name',
            'instance_role' => 'Instance Role',
            'instance_status' => 'Instance Status',
            'database_role' => 'Database Role',
            'open_mode' => 'Open Mode',
            'protection_mode' => 'Protection Mode',
            'host_name' => 'Host Name',
            'database_status' => 'Database Status',
            'startup_time' => 'Startup Time',
            'uptime' => 'Uptime',
            'version' => 'Version',
            'archiver' => 'Archiver',
            'session_total' => 'Session Total',
            'session_actives' => 'Session Actives',
            'session_waits' => 'Session Waits',
            'dg_stats' => 'Dg Stats',
            'dg_delay' => 'Dg Delay',
            'processes' => 'Processes',
            'session_logical_reads_persecond' => 'Session Logical Reads Persecond',
            'physical_reads_persecond' => 'Physical Reads Persecond',
            'physical_writes_persecond' => 'Physical Writes Persecond',
            'physical_read_io_requests_persecond' => 'Physical Read Io Requests Persecond',
            'physical_write_io_requests_persecond' => 'Physical Write Io Requests Persecond',
            'db_block_changes_persecond' => 'Db Block Changes Persecond',
            'os_cpu_wait_time' => 'Os Cpu Wait Time',
            'logons_persecond' => 'Logons Persecond',
            'logons_current' => 'Logons Current',
            'opened_cursors_persecond' => 'Opened Cursors Persecond',
            'opened_cursors_current' => 'Opened Cursors Current',
            'user_commits_persecond' => 'User Commits Persecond',
            'user_rollbacks_persecond' => 'User Rollbacks Persecond',
            'user_calls_persecond' => 'User Calls Persecond',
            'db_block_gets_persecond' => 'Db Block Gets Persecond',
            'create_time' => 'Create Time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'ymdhi' => 'Ymdhi',
        ];
    }

    public function getServers()
    {
        return $this->hasone(\backend\modules\config\models\OracleServer::className(), ['host' => 'host','port'=>'port']);
    }

}
