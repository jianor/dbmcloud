<?php

namespace backend\modules\oracle\models;

use backend\modules\config\models\OracleServer;
use Yii;

/**
 * This is the model class for table "oracle_tablespace_history".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property string $tablespace_name
 * @property string $total_size
 * @property string $used_size
 * @property string $avail_size
 * @property string $used_rate
 * @property string $create_time
 * @property string $ymdhi
 */
class OracleTablespaceHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_tablespace_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tablespace_name'], 'required'],
            [['total_size', 'used_size', 'avail_size', 'ymdhi'], 'integer'],
            [['create_time'], 'safe'],
            [['host', 'tags'], 'string', 'max' => 50],
            [['port'], 'string', 'max' => 30],
            [['tablespace_name'], 'string', 'max' => 100],
            [['used_rate'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Host',
            'port' => 'Port',
            'tags' => 'Tags',
            'tablespace_name' => 'Tablespace Name',
            'total_size' => 'Total Size',
            'used_size' => 'Used Size',
            'avail_size' => 'Avail Size',
            'used_rate' => 'Used Rate',
            'create_time' => 'Create Time',
            'ymdhi' => 'Ymdhi',
        ];
    }


}
