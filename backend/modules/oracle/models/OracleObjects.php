<?php

namespace backend\modules\oracle\models;

use Yii;

/**
 * This is the model class for table "oracle_dba_objects".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property string $owner
 * @property string $object_name
 * @property string $object_type
 * @property string $status
 * @property string $create_time
 */
class OracleObjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_dba_objects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner'], 'required'],
            [['create_time'], 'safe'],
            [['host', 'tags', 'object_type', 'status'], 'string', 'max' => 50],
            [['port'], 'string', 'max' => 30],
            [['owner', 'object_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'tags' => '标签',
            'owner' => '隶属用户',
            'object_name' => '对象名称',
            'object_type' => '对象类型',
            'status' => '状态',
            'create_time' => '采集时间',
        ];
    }
}
