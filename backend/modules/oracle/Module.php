<?php

namespace backend\modules\oracle;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\oracle\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
