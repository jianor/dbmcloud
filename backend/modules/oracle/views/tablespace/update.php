<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleTablespace */

$this->title = 'Update Oracle Tablespace: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oracle Tablespaces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="oracle-tablespace-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
