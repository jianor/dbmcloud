<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .' TBS:'.$model->tablespace_name. ']';
$this->params['breadcrumbs'][] = ['label' => 'Oracle 表空间监控', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host.':'.$model->port;
?>


<div class="container-fluid mt15">
    <div class="col-xs-5 text-left">
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/tablespace/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'3600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==3600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/tablespace/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'21600','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==21600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">6小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/tablespace/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'43200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==43200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">12小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/tablespace/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'86400','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==86400): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1天</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['oracle/tablespace/chart/','server'=>$model->host.'-'.$model->port,'time_interval'=>'259200','end'=>'0'  ]); ?>" class="btn <?php if($time_interval==259200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">3天</a>
    </div>
    <?php $form = ActiveForm::begin(['action'=>['tablespace/chart'],'method'=>'post']); ?>
    <div class="col-xs-5 text-left">

        <?= DateRangePicker::widget([
            'name' => 'time_from',
            'value' => $time_from,
            'nameTo' => 'time_to',
            'valueTo' => $time_to,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-d'
            ]
        ]);?>

    </div>
    <div class="col-xs-1 text-left">
        <?= Html::activeHiddenInput($model,'host'); ?>
        <?= Html::activeHiddenInput($model,'port'); ?>
        <?= Html::submitButton('生成图表', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<hr/>
<div class="container-fluid mt15">
    <div id="main_chart_tablespace" class="col-xs-12 text-center line-chart"></div>
</div>



<?php \common\widgets\JsBlock::begin() ?>

<script type="text/javascript">
    //连接状态
    var myChart = echarts.init(document.getElementById('main_chart_tablespace'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933','#FF3333'],
        title : {
            text: '表空间使用率',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['总计','已用百分比'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'总计',
                type:'line',
                smooth:true,
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = 100;
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'已用百分比',
                type:'line',
                smooth:true,
                itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['used_rate'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>


<?php \common\widgets\JsBlock::end()?>


