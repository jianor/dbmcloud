<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\oracle\models\OracleStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'tags') ?>

    <?= $form->field($model, 'connect') ?>

    <?php // echo $form->field($model, 'instance_name') ?>

    <?php // echo $form->field($model, 'instance_role') ?>

    <?php // echo $form->field($model, 'instance_status') ?>

    <?php // echo $form->field($model, 'database_role') ?>

    <?php // echo $form->field($model, 'open_mode') ?>

    <?php // echo $form->field($model, 'protection_mode') ?>

    <?php // echo $form->field($model, 'host_name') ?>

    <?php // echo $form->field($model, 'database_status') ?>

    <?php // echo $form->field($model, 'startup_time') ?>

    <?php // echo $form->field($model, 'uptime') ?>

    <?php // echo $form->field($model, 'version') ?>

    <?php // echo $form->field($model, 'archiver') ?>

    <?php // echo $form->field($model, 'session_total') ?>

    <?php // echo $form->field($model, 'session_actives') ?>

    <?php // echo $form->field($model, 'session_waits') ?>

    <?php // echo $form->field($model, 'dg_stats') ?>

    <?php // echo $form->field($model, 'dg_delay') ?>

    <?php // echo $form->field($model, 'processes') ?>

    <?php // echo $form->field($model, 'session_logical_reads_persecond') ?>

    <?php // echo $form->field($model, 'physical_reads_persecond') ?>

    <?php // echo $form->field($model, 'physical_writes_persecond') ?>

    <?php // echo $form->field($model, 'physical_read_io_requests_persecond') ?>

    <?php // echo $form->field($model, 'physical_write_io_requests_persecond') ?>

    <?php // echo $form->field($model, 'db_block_changes_persecond') ?>

    <?php // echo $form->field($model, 'os_cpu_wait_time') ?>

    <?php // echo $form->field($model, 'logons_persecond') ?>

    <?php // echo $form->field($model, 'logons_current') ?>

    <?php // echo $form->field($model, 'opened_cursors_persecond') ?>

    <?php // echo $form->field($model, 'opened_cursors_current') ?>

    <?php // echo $form->field($model, 'user_commits_persecond') ?>

    <?php // echo $form->field($model, 'user_rollbacks_persecond') ?>

    <?php // echo $form->field($model, 'user_calls_persecond') ?>

    <?php // echo $form->field($model, 'db_block_gets_persecond') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
