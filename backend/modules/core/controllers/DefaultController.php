<?php

namespace backend\modules\core\controllers;

use backend\controllers\BaseController;
use backend\models\Menu;
use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['module/index']);
        //return $this->render('index');
    }

    public function  actionAjaxSelectedModule($id)
    {
        $item = Menu::find()->where(['id'=>$id])->one();
        echo !empty($item->module_id) ? $item->module_id : 0;
    }
}
