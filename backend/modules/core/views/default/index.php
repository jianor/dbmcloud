<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;


$this->title = '核心';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sys-default-index">

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <td colspan="4"><b>统计信息</b></td>
        </tr>
        <tr>
            <td>企业总数</td>
            <td></td>
            <td>付费总数</td>
            <td></td>
        </tr>
        <tr>
            <td>本周新增企业数</td>
            <td></td>
            <td>本月新增企业数</td>
            <td></td>
        </tr>
    </table>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <td colspan="4"><b>快捷菜单</b></td>
        </tr>
        <tr>
            <td>
                <div class="icons">
                    <ul class="icon_lists clear">

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('core/module/index'); ?>"> <i class="icon iconfont">&#xe710;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('core/module/index'); ?>">模块管理</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('core/menu/index'); ?>"> <i class="icon iconfont">&#xe708;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('core/menu/index'); ?>"> 菜单管理</a></div>
                        </li>


                    </ul>
                </div>
            </td>
        </tr>

        </tbody>
    </table>





</div>
