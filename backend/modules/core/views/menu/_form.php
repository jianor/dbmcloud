<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Menu;
use backend\models\Module;
use common\models\Base;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(
        ArrayHelper::merge([0 => Yii::t('app', 'Root Menu')], ArrayHelper::map(Menu::getTree(0, Menu::find()->asArray()->all()), 'id', 'name')),
        [
            //'prompt' => Yii::t('app', 'Please Select'),
            'onchange' => '
            $.post( "'.Yii::$app->urlManager->createUrl('core/default/ajax-selected-module?id=').'"+$(this).val(),function(data){
                $("#menu-module_id").val(data);
            });
            ',
        ]
    ) ?>

    <?= $form->field($model, 'module_id')->dropDownList(ArrayHelper::map(Module::findTotal(), 'id', 'title'),['prompt' => Yii::t('app', 'Please Select'),]) ?>

    <?= $form->field($model, 'menu_name')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'route')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'menu_icon')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'display')->dropDownList((Base::getYnStatus())) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
