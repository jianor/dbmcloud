<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Module */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-view">

    <p>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'title',
            'description',
            ['label'=>Yii::t('app','Private'),'value'=> $model->private==1 ? Yii::t('app','YES') : Yii::t('app','NO')],
            ['label'=>Yii::t('app','System'),'value'=> $model->system==1 ? Yii::t('app','YES') : Yii::t('app','NO')],
            ['label'=>Yii::t('app','Status'),'value'=> $model->status==1 ? Yii::t('app','STATUS_ACTIVE') : Yii::t('app','STATUS_INACTIVE')],
            'price',
            'promotion_price',
            'sort_order',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
