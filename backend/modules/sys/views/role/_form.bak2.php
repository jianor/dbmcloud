<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;

$auth = Yii::$app->authManager;
$user = Yii::$app->user;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <?php
    $form = ActiveForm::begin([
        'enableClientValidation' => true,
        /*'enableAjaxValidation' => true,
        'validateOnSubmit'=>true,
        'validateOnChange' => false*/
    ]);
    ?>
    
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Yii::t('app', 'Role'); ?>
            </div>
            <div class="panel-body">
                <?php
                echo //$form->field($model, 'name')->textInput($model->isNewRecord ? [] : ['disabled' => 'disabled']) .
                     $form->field($model, 'description')->textInput() .
                     Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), [
                        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
                     ]);
                ?>
                <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
            </div>

        </div>
    </div>

    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Yii::t('app', 'Permissions'); ?>
            </div>

            <div class="panel-body">
                 
                 <table id="sample-table-1" class="table table-bordered">
        <thead>
        <tr>
            <th class="hidden-320">一级菜单</th>
            <th>二级菜单</th>
          
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($permissions as $f): ?>
            <tr>
                <td style="width: 150px;">
                    <input type="checkbox"  name="<?= $f['id'] ?>" id="<?= $f['id'] ?>" onclick="ckbox(1,this)" value="<?= $f['route'] ?>" <?php if (isset($role) && $auth->hasChild($auth->getRole($role),$auth->getPermission($f->route))): ?> checked <?php endif; ?> />
                    &nbsp;<?= $f['menu_name'] ?></td>
                <td>
                   <?php foreach ($f->getSon()->all() as $son): ?>
                   		<div class="panel panel-default">
                                <div class="panel-heading">
                                        <input type="checkbox" name="<?= $f['id'] . '_' . $son['id'] ?>" id="<?= $son['id'] ?>" onclick="ckbox(2,this)"  value="<?= $son['route'] ?>"  <?php if (isset($role) && $auth->hasChild($auth->getRole($role),$auth->getPermission($son->route))): ?> checked <?php endif; ?> />
                                        <?= $son['menu_name'] ?>
                                </div>
                                <div class="panel-body">
    								 <?php foreach ($son->getSon()->all() as $gson): ?>
                                                <input type="checkbox"
                                                       name="<?= $f['id'] . '_' . $son['id'] . '_' . $gson['id'] ?>"
                                                       id="<?= $gson['id'] ?>"
                                                       onclick="ckbox(3,this)"
                                                   	   value="<?= $gson['route'] ?>"
                                                        <?php if (isset($role) && $auth->hasChild($auth->getRole($role),$auth->getPermission($gson->route))): ?> checked <?php endif; ?>
                                                   />&nbsp;
                                                <?= $gson['menu_name'] ?>
                                     <?php endforeach; ?>
  								</div>
                                
                    	</div>
                   <?php endforeach; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
                 
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script>
    function ckbox(level, o) {
        var name = $(o).attr('name');
        var id = $(o).attr('id');
        var val = $(o).val();
        var thischecked = $(o).is(':checked');
        //选中所有子孙
        $('input[name*=' + name + '_]').prop('checked', thischecked);
        //取消选中时判断父节点
        var arr = name.split('_');
        if (level == 3) {
            //如果3级菜单全都没选中，对应的2级菜单也取消选中
            var cntlv3 = $('input[name*=' + arr[0] + '_' + arr[1] + '_]:checked').size();
            if (cntlv3 > 0) {
                $('input[name=' + arr[0] + '_' + arr[1] + ']').prop('checked', true);
            } else {
                $('input[name=' + arr[0] + '_' + arr[1] + ']').prop('checked', false);
            }
        }
        if (level >= 2) {
            //如果2级菜单都没选中 1级菜单也取消选中
            var cntlv2 = $('input[name*=' + arr[0] + '_' + ']:checked').size();
            if (cntlv2 > 0) {
                $('#' + arr[0]).prop('checked', true);
            } else {
                $('#' + arr[0]).prop('checked', false);
            }
        }

       
    }
</script>