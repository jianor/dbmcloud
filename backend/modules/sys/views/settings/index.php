<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Organization;

/* @var $this yii\web\View */
/* @var $model common\models\Organization */
/* @var $form ActiveForm */

$this->title = '后台设置';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="settings-index">
    <?php $form = ActiveForm::begin(); ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- organization-index -->
