<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Position */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-view">

    <p>
        <?= Html::a(Yii::t('app', 'Return'), ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::button(Yii::t('app', 'Print'), ['class' => 'btn btn-info','onclick'=>'preview()']) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'uuid' => $model->uuid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'uuid' => $model->uuid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <!--startprint-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uuid',
            'name',
            ['label'=>Yii::t('app','Status'),'value'=> $model->status==1 ? Yii::t('app','STATUS_ACTIVE') : Yii::t('app','STATUS_INACTIVE')],
            'sort_order',
            'created_at:datetime',
            'updated_at:datetime',
            ['label'=>Yii::t('app','Created By'),'value'=>\backend\models\User::getRealname($model->created_by)],
            ['label'=>Yii::t('app','Updated By'),'value'=>\backend\models\User::getRealname($model->updated_by)],
        ],
    ]) ?>
    <!--endprint-->

</div>
