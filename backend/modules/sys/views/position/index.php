<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Base;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Positions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Position'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'status',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->status==1 ? Yii::t('app','STATUS_ACTIVE') : Yii::t('app','STATUS_INACTIVE');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    Base::getActiveStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '110'],

            ],
            [
                'attribute' => 'sort_order',
                'headerOptions' => ['width' => '75'],
            ],
            [
                'attribute' => 'created_by',
                'format' => 'html',
                'value' =>
                    function ($model){
                        return User::getRealname($model->created_by);
                    },
                'headerOptions' => ['width' => '100'],
            ],
            [
                'attribute' => 'updated_by',
                'format' => 'html',
                'value' =>
                    function ($model){
                        return User::getRealname($model->updated_by);
                    },
                'headerOptions' => ['width' => '100'],
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'label'=>Yii::t('app', 'Operate'),
                'headerOptions' => ['width'=>'85'],
                'format'=>'raw',
                'value' => function($model){
                    return
                        Html::a('<span class="glyphicon glyphicon-eye-open"></span>',Yii::$app->getUrlManager()->createUrl(['/sys/position/view','uuid'=>$model['uuid']]), ['title' => Yii::t('app','View') ]).'&nbsp'
                        .Html::a('<span class="glyphicon glyphicon-pencil"></span>',Yii::$app->getUrlManager()->createUrl(['/sys/position/update','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Update') ]).'&nbsp'
                        .Html::a('<span class="glyphicon glyphicon-trash"></span>',Yii::$app->getUrlManager()->createUrl(['/sys/position/delete','uuid'=>$model['uuid']]), ['title' => Yii::t('app','Delete') , 'data-method' => 'post','data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')] );
                }
            ],
        ],
    ]); ?>

</div>
