<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Position */

$this->title = Yii::t('app', 'Create ') . Yii::t('app', 'Position');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Positions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
