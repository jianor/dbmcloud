<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = Yii::t('app', 'Update ') . Yii::t('app', 'Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oa-password-index">

        <?php $form = ActiveForm::begin(); ?>


                    <div class="user-form">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'readonly' => true]) ?>

                        <?= $form->field($model, 'realname')->textInput(['maxlength' => 255, 'readonly' => true]) ?>

                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'repassword')->passwordInput(['maxlength' => 255]) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>

        <?php ActiveForm::end(); ?>


</div>