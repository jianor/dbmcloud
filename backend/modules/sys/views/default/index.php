<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Base;
use backend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '系统';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sys-default-index">

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <td colspan="4"><b>企业信息 [<a href="<?=Yii::$app->urlManager->createUrl('sys/organization/index'); ?>">编辑信息</a> ]</b></td>
        </tr>
        <tr>
            <td>单位名称</td>
            <td><?= $model->name; ?></td>
            <td>通讯地址</td>
            <td><?= $model->address; ?></td>
        </tr>
        <tr>
            <td>电话号码</td>
            <td><?= $model->phone; ?></td>
            <td>传真号码</td>
            <td><?= $model->fax; ?></td>
        </tr>
        <tr>
            <td>电子邮箱</td>
            <td><?= $model->email; ?></td>
            <td>单位网址</td>
            <td><?= $model->url; ?></td>
        </tr>
    </table>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <td colspan="4"><b>快捷菜单</b></td>
        </tr>
        <tr>
            <td>
                <div class="icons">
                    <ul class="icon_lists clear">

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/organization/index'); ?>"> <i class="icon iconfont">&#xe710;</i></a>
                             <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/organization/index'); ?>">单位管理</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/department/index'); ?>"> <i class="icon iconfont">&#xe708;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/department/index'); ?>"> 部门管理</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/position/index'); ?>"><i class="icon iconfont">&#xe742;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/position/index'); ?>">职位管理</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/role/index'); ?>"><i class="icon iconfont">&#xe699;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/role/index'); ?>">角色管理</a></div>
                        </li>
                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/user/index'); ?>"><i class="icon iconfont">&#xe614;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/user/index'); ?>">用户管理</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/settings/backend'); ?>"><i class="icon iconfont">&#xe7dc;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/settings/backend'); ?>">后台设置</a></div>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('sys/action-log/index'); ?>"><i class="icon iconfont">&#xe827;</i></a>
                            <div class="name"><a href="<?=Yii::$app->urlManager->createUrl('sys/action-log/index'); ?>">操作日志</a></div>
                        </li>


                    </ul>
                </div>
            </td>
        </tr>

        </tbody>
    </table>





</div>
