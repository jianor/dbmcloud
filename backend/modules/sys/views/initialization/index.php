<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Base;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Initialization');
$this->params['breadcrumbs'][] = Yii::t('app', 'Initialization');

?>

<!-- START TYPOGRAPHY -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-cog"></i>
                <h3 class="box-title"><?=Yii::t('app', 'Initialization') ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <h5><li>欢迎使用基础数据初始化功能。在使用系统前必须进行初始化。初始化数据可以将云平台公共库的数据初始化到您企业的企业库里。</li></h5>
                <h5><li>初始化的数据包括：客户等级、客户来源、供应商等级、行业分类、经济类型、销售方式、销售阶段、证照类型、计量单位、财务收入类型、财务支出类型。</li></h5>
                <h5><li>在未初始化之前，您无权进行上述基础数据表的操作。</li></h5>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- ./col -->
</div><!-- /.row -->

<div>
    <?php if($initialization==0){
        echo Html::a(Yii::t('app', 'Initialization'), ['initialization/execute'], ['class' => 'btn btn-warning btn-block btn-flat']);
    }
    elseif($initialization==1){
        echo Html::button(Yii::t('app', 'You Already Initialization'), ['class' => 'btn btn-warning btn-block btn-flat disabled']);
    }
    ?>
</div>
