<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\helpers\Tools;
use leandrogehlen\treegrid\TreeGrid;
use common\models\Department;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Departments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Department'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>




    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'Department') ?> </th>
            <th><?= Yii::t('app', 'Operate') ?></th>

        </tr>
        </thead>
        <tbody>
        <?php if(count($dataProvider)>0): ?>
        <?php foreach($dataProvider as $item){ ?>
            <tr data-key="1">
                <td><?= $item['name']; ?></td>
                <td>
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['/sys/department/view','uuid'=>$item['uuid']]); ?>"" title="<?= Yii::t('app', 'View');?>" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['/sys/department/update','uuid'=>$item['uuid']]); ?>"" title="<?= Yii::t('app', 'Update');?>" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="<?= \Yii::$app->getUrlManager()->createUrl(['/sys/department/delete','uuid'=>$item['uuid']]); ?>"" title="<?= Yii::t('app', 'Delete');?>" data-confirm="<?= Yii::t('app', 'Are you sure you want to delete this item?');?>" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <?php } ?>
        <?php else: ?>
            <tr><td colspan="3"><?= Yii::t('app','You Are No Department Yet!') ?> <?= Html::a(Yii::t('app', 'Create ') . Yii::t('app', 'Department'), ['create'], ['class' => 'btn btn-warning btn-flat btn-xs']) ?></td></tr>
        <?php endif; ?>
        </tbody>
    </table>

</div>
