<?php

namespace backend\modules\sys\controllers;

use common\models\GoodsUnit;
use common\models\CertType;
use common\models\CustomerLevel;
use common\models\CustomerSource;
use common\models\EconomicType;
use common\models\IndustryType;
use common\models\Organization;
use common\models\SalesMethod;
use common\models\SalesStage;
use common\models\SupplierLevel;
use Yii;
use yii\web\Controller;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\ActionLog;
use backend\models\SourceData;
use c006\alerts\Alerts;

class InitializationController extends Controller
{
    public function actionIndex()
    {
        $initialization = Organization::getInfo()->initialization;
        return $this->render('index',['initialization'=>$initialization]);
    }

    public function actionExecute()
    {
        //check initialization
        $initialization = Organization::getInfo()->initialization;
        if($initialization==1){
            return $this->render('index',['initialization'=>$initialization]);exit;
        }


        $created_by = Yii::$app->user->id;
        $updated_by = Yii::$app->user->id;
        $organization_id = Yii::$app->user->identity->organization_id;


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            //load customer_level
            $customer_level = SourceData::filterSourceData('customer_level');
            foreach($customer_level as $item){
                $model = new CustomerLevel();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }
            //load customer_source
            $customer_source = SourceData::filterSourceData('customer_source');
            foreach($customer_source as $item){
                $model = new CustomerSource();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }
            //load industry_type
            $industry_type = SourceData::filterSourceData('industry_type');
            foreach($industry_type as $item){
                $model = new IndustryType();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }
            //load economic_type
            $economic_type = SourceData::filterSourceData('economic_type');
            foreach($economic_type as $item){
                $model = new EconomicType();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            //load sales_method
            $sales_method = SourceData::filterSourceData('sales_method');
            foreach($sales_method as $item){
                $model = new SalesMethod();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            //load supplier_level
            $supplier_level = SourceData::filterSourceData('supplier_level');
            foreach($supplier_level as $item){
                $model = new SupplierLevel();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            //load goods_unit
            $goods_unit = SourceData::filterSourceData('goods_unit');
            foreach($goods_unit as $item){
                $model = new GoodsUnit();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            //load cert_type
            $cert_type = SourceData::filterSourceData('cert_type');
            foreach($cert_type as $item){
                $model = new CertType();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            //load sales_stage
            $sales_stage = SourceData::filterSourceData('sales_stage');
            foreach($sales_stage as $item){
                $model = new SalesStage();
                $model->name = $item['name'];
                $model->status=1;
                $model->sort_order = $item['sort_order'];
                if (false === $model->save())
                {
                    throw new Exception('save_error');
                }
            }

            $connection->createCommand("update organization set initialization=1 where id=$organization_id")->execute();
            $transaction->commit();
            ActionLog::log("初始化基础数据:成功");
            Alerts::setMessage('初始化基础数据成功!');
            Alerts::setAlertType(Alerts::ALERT_SUCCESS);

        } catch(Exception $e) {

            $transaction->rollBack();
            ActionLog::log("初始化基础数据:失败");
            Alerts::setMessage('初始化基础数据失败!');
            Alerts::setAlertType(Alerts::ALERT_DANGER);
        }

        return $this->redirect(['index']);
    }

}
