<?php

namespace backend\modules\sys\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\User;
use backend\models\UserSearch;
use common\models\Department;
use common\models\Position;


class LayerController extends Controller
{
    public function actionUser()
    {
        $this->layout = 'layer';

        $object_id = Yii::$app->request->get('object_id');
        $layer_user_tab = !empty(Yii::$app->request->get('layer_user_tab')) ? Yii::$app->request->get('layer_user_tab') : 'department';
        $department = Department::getTree(0, Department::find()->where(['organization_id'=>Yii::$app->user->identity->organization_id])->asArray()->all());
        $position = Position::find()->where(['organization_id'=>Yii::$app->user->identity->organization_id,'status'=>1])->orderBy('sort_order ASC')->asArray()->all();
        $searchModel = new UserSearch();
        //print_r(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user', [
            'object_id' => $object_id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'department' => $department,
            'position' => $position,
            'layer_user_tab' => $layer_user_tab,
        ]);
    }
}
