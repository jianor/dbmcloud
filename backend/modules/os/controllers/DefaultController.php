<?php

namespace backend\modules\os\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->redirect(['status/index']);
        //return $this->render('index');
    }
}
