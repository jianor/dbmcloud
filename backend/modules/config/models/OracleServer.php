<?php

namespace backend\modules\config\models;

use Yii;

/**
 * This is the model class for table "oracle_server".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $dsn
 * @property string $username
 * @property string $password
 * @property string $tags
 * @property integer $monitor
 * @property integer $send_mail
 * @property string $send_mail_to_list
 * @property integer $send_sms
 * @property string $send_sms_to_list
 * @property integer $alarm_session_total
 * @property integer $alarm_session_actives
 * @property integer $alarm_session_waits
 * @property integer $alarm_tablespace
 * @property integer $threshold_session_total
 * @property integer $threshold_session_actives
 * @property integer $threshold_session_waits
 * @property integer $threshold_tablespace
 * @property integer $created_at
 * @property integer $updated_at
 */
class OracleServer extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'oracle_server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['host', 'port','username', 'password','tags','dsn','is_rac'], 'required'],
            [['monitor','is_rac', 'send_mail', 'send_sms', 'alarm_session_total', 'alarm_session_actives', 'alarm_session_waits', 'alarm_tablespace','alarm_objects','alarm_index','alarm_hard_parse','alarm_sqlmonitor','alarm_rollstat','alarm_temp_tbs','alarm_data_dict_rate','alarm_buffer_pool_rate','alarm_table_hwm','alarm_white_list', 'threshold_session_total', 'threshold_session_actives', 'threshold_session_waits', 'threshold_tablespace','threshold_objects','threshold_index','threshold_hard_parse','threshold_sqlmonitor','threshold_rollstat','threshold_temp_tbs','threshold_data_dict_rate','threshold_buffer_pool_rate','threshold_table_hwm','threshold_white_list', 'created_at', 'updated_at'], 'integer'],
            [['host'], 'string', 'max' => 30],
            [['port'], 'string', 'max' => 10],
            [['dsn', 'username'], 'string', 'max' => 50],
            [['password', 'tags'], 'string', 'max' => 100],
            [['send_mail_to_list', 'send_sms_to_list'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'username' => '用户',
            'password' => '密码',
            'tags' => '标签',
            'dsn' => 'DSN',
            'monitor' => '监控',
            'send_mail' => '邮件通知',
            'send_mail_to_list' => '邮件收件人列表',
            'send_sms' => '发送短信',
            'send_sms_to_list' => '短信收件人列表',
            'alarm_session_total' => '连接会话数告警',
            'alarm_session_actives' => '活动会话数告警',
            'alarm_session_waits' => '等待会话数告警',
            'alarm_tablespace' => '表空间告警',
            'alarm_objects' => '无效对象告警',
            'alarm_index' => '失效索引告警',
            'alarm_hard_parse' => '硬解析告警',
            'alarm_sqlmonitor' => 'SQL MONITOR 告警',
            'alarm_rollstat' => '回滚段告警',
            'alarm_temp_tbs' => '临时表空间告警',
            'alarm_data_dict_rate' => '数据字典命中率告警',
            'alarm_buffer_pool_rate' => 'SGA缓存命中率告警',
            'alarm_table_hwm' => 'HWM高水位表告警',
            'alarm_white_list' => '连接客户端异常告警',


            'threshold_session_total' => '连接会话数阀值',
            'threshold_session_actives' => '活动会话数阀值',
            'threshold_session_waits' => '等待会话数阀值',
            'threshold_tablespace' => '表空间阀值',
            'threshold_objects' => '无效对象告警阀值数',
            'threshold_index' => '失效索引告警阀值数',
            'threshold_hard_parse' => '硬解析阀值数',
            'threshold_sqlmonitor' => 'SQL MONITOR阀值数',
            'threshold_rollstat' => '回滚段阀值数',
            'threshold_temp_tbs' => '临时表空间阀值数',
            'threshold_data_dict_rate' => '数据字典命中率阀值数',
            'threshold_buffer_pool_rate' => 'SGA缓存命中率阀值数',
            'threshold_table_hwm' => 'HWM高水位表阀值(碎片率%)',
            'threshold_white_list' => '连接客户端白名单',
            'is_rac' => '是否RAC节点',
            'created_at' => '创建时间',
            'updated_at' => '更新时间',

        ];
    }
}
