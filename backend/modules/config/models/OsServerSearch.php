<?php

namespace backend\modules\config\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\config\models\OsServer;

/**
 * OsServerSearch represents the model behind the search form about `backend\modules\config\models\OsServer`.
 */
class OsServerSearch extends OsServer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitor', 'send_mail', 'send_sms', 'alarm_os_process', 'alarm_os_load', 'alarm_os_cpu', 'alarm_os_network', 'alarm_os_disk', 'alarm_os_memory', 'threshold_os_process', 'threshold_os_load', 'threshold_os_cpu', 'threshold_os_network', 'threshold_os_disk', 'threshold_os_memory'], 'integer'],
            [['host', 'community', 'tags', 'send_mail_to_list', 'send_sms_to_list', 'filter_os_disk', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OsServer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'monitor' => $this->monitor,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
            'alarm_os_process' => $this->alarm_os_process,
            'alarm_os_load' => $this->alarm_os_load,
            'alarm_os_cpu' => $this->alarm_os_cpu,
            'alarm_os_network' => $this->alarm_os_network,
            'alarm_os_disk' => $this->alarm_os_disk,
            'alarm_os_memory' => $this->alarm_os_memory,
            'threshold_os_process' => $this->threshold_os_process,
            'threshold_os_load' => $this->threshold_os_load,
            'threshold_os_cpu' => $this->threshold_os_cpu,
            'threshold_os_network' => $this->threshold_os_network,
            'threshold_os_disk' => $this->threshold_os_disk,
            'threshold_os_memory' => $this->threshold_os_memory,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'send_mail_to_list', $this->send_mail_to_list])
            ->andFilterWhere(['like', 'send_sms_to_list', $this->send_sms_to_list])
            ->andFilterWhere(['like', 'filter_os_disk', $this->filter_os_disk]);

        return $dataProvider;
    }
}
