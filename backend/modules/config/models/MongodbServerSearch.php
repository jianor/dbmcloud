<?php

namespace backend\modules\config\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\config\models\MongodbServer;

/**
 * MongodbServerSearch represents the model behind the search form about `backend\modules\config\models\MongodbServer`.
 */
class MongodbServerSearch extends MongodbServer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitor', 'send_mail', 'send_sms', 'alarm_connections_current', 'alarm_active_clients', 'alarm_current_queue', 'threshold_connections_current', 'threshold_active_clients', 'threshold_current_queue'], 'integer'],
            [['host', 'port', 'username', 'password', 'tags', 'send_mail_to_list', 'send_sms_to_list', 'create_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MongodbServer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'monitor' => $this->monitor,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
            'alarm_connections_current' => $this->alarm_connections_current,
            'alarm_active_clients' => $this->alarm_active_clients,
            'alarm_current_queue' => $this->alarm_current_queue,
            'threshold_connections_current' => $this->threshold_connections_current,
            'threshold_active_clients' => $this->threshold_active_clients,
            'threshold_current_queue' => $this->threshold_current_queue,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'send_mail_to_list', $this->send_mail_to_list])
            ->andFilterWhere(['like', 'send_sms_to_list', $this->send_sms_to_list]);

        return $dataProvider;
    }
}
