<?php

namespace backend\modules\config\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\config\models\OracleServer;

/**
 * OracleServerSearch represents the model behind the search form about `backend\modules\config\models\OracleServer`.
 */
class OracleServerSearch extends OracleServer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitor', 'send_mail', 'send_sms', 'alarm_session_total', 'alarm_session_actives', 'alarm_session_waits', 'alarm_tablespace', 'threshold_session_total', 'threshold_session_actives', 'threshold_session_waits', 'threshold_tablespace', 'created_at', 'updated_at'], 'integer'],
            [['host', 'port', 'dsn', 'username', 'password', 'tags', 'send_mail_to_list', 'send_sms_to_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OracleServer::find();

        $query->orderBy(['tags' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'monitor' => $this->monitor,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
            'alarm_session_total' => $this->alarm_session_total,
            'alarm_session_actives' => $this->alarm_session_actives,
            'alarm_session_waits' => $this->alarm_session_waits,
            'alarm_tablespace' => $this->alarm_tablespace,
            'threshold_session_total' => $this->threshold_session_total,
            'threshold_session_actives' => $this->threshold_session_actives,
            'threshold_session_waits' => $this->threshold_session_waits,
            'threshold_tablespace' => $this->threshold_tablespace,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'dsn', $this->dsn])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'send_mail_to_list', $this->send_mail_to_list])
            ->andFilterWhere(['like', 'send_sms_to_list', $this->send_sms_to_list]);

        return $dataProvider;
    }
}
