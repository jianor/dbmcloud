<?php

namespace backend\modules\config\models;

use Yii;

/**
 * This is the model class for table "os_server".
 *
 * @property integer $id
 * @property string $host
 * @property string $community
 * @property string $tags
 * @property integer $monitor
 * @property integer $send_mail
 * @property string $send_mail_to_list
 * @property integer $send_sms
 * @property string $send_sms_to_list
 * @property integer $alarm_os_process
 * @property integer $alarm_os_load
 * @property integer $alarm_os_cpu
 * @property integer $alarm_os_network
 * @property integer $alarm_os_disk
 * @property integer $alarm_os_memory
 * @property integer $threshold_os_process
 * @property integer $threshold_os_load
 * @property integer $threshold_os_cpu
 * @property integer $threshold_os_network
 * @property integer $threshold_os_disk
 * @property integer $threshold_os_memory
 * @property string $filter_os_disk
 * @property string $create_time
 */
class OsServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'os_server';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['monitor', 'send_mail', 'send_sms', 'alarm_os_process', 'alarm_os_load', 'alarm_os_cpu', 'alarm_os_network', 'alarm_os_disk', 'alarm_os_memory', 'threshold_os_process', 'threshold_os_load', 'threshold_os_cpu', 'threshold_os_network', 'threshold_os_disk', 'threshold_os_memory'], 'integer'],
            [['create_time'], 'safe'],
            [['host', 'tags'], 'string', 'max' => 30],
            [['community'], 'string', 'max' => 50],
            [['send_mail_to_list', 'send_sms_to_list'], 'string', 'max' => 255],
            [['filter_os_disk'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'community' => '团体名',
            'tags' => '标签',
            'monitor' => '监控',
            'send_mail' => '邮件通知',
            'send_mail_to_list' => '邮件收件人列表',
            'send_sms' => '发送短信',
            'send_sms_to_list' => '短信收件人列表',
            'alarm_os_process' => '进程数',
            'alarm_os_load' => '主机负载',
            'alarm_os_cpu' => 'CPU使用率',
            'alarm_os_network' => '网络流量',
            'alarm_os_disk' => '磁盘',
            'alarm_os_memory' => '内存',
            'threshold_os_process' => '进程数',
            'threshold_os_load' => '主机负载',
            'threshold_os_cpu' => 'CPU使用率',
            'threshold_os_network' => '网络流量',
            'threshold_os_disk' => '磁盘',
            'threshold_os_memory' => '内存',
            'filter_os_disk' => '磁盘过滤',
            'create_time' => 'Create Time',
        ];
    }
}
