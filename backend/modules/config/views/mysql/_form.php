<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MysqlServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mysql-server-form">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">连接信息</div>
                <div class="panel-body">
                    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警通知</div>
                <div class="panel-body">

                    <?= $form->field($model, 'monitor')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_sms')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail_to_list')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'send_sms_to_list')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警项目</div>
                <div class="panel-body">

                    <?= $form->field($model, 'alarm_threads_connected')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_threads_running')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_threads_waits')->dropDownList((\common\models\Base::getOnStatus())) ?>
                    <!--
                    <?= $form->field($model, 'alarm_repl_status')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_repl_delay')->dropDownList((\common\models\Base::getOnStatus())) ?>
                    -->
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警阀值</div>
                <div class="panel-body">

                    <?= $form->field($model, 'threshold_threads_connected')->textInput() ?>

                    <?= $form->field($model, 'threshold_threads_running')->textInput() ?>

                    <?= $form->field($model, 'threshold_threads_waits')->textInput() ?>
                    <!--
                    <?= $form->field($model, 'threshold_repl_delay')->textInput() ?>
                    -->
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('返回', ['index'], ['class' => 'btn btn-warning']) ?>
                    </div>

                </div>

            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
