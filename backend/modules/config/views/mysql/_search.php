<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MysqlServerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mysql-server-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'monitor') ?>

    <?php // echo $form->field($model, 'send_mail') ?>

    <?php // echo $form->field($model, 'send_mail_to_list') ?>

    <?php // echo $form->field($model, 'send_sms') ?>

    <?php // echo $form->field($model, 'send_sms_to_list') ?>

    <?php // echo $form->field($model, 'send_slowquery_to_list') ?>

    <?php // echo $form->field($model, 'alarm_threads_connected') ?>

    <?php // echo $form->field($model, 'alarm_threads_running') ?>

    <?php // echo $form->field($model, 'alarm_threads_waits') ?>

    <?php // echo $form->field($model, 'alarm_repl_status') ?>

    <?php // echo $form->field($model, 'alarm_repl_delay') ?>

    <?php // echo $form->field($model, 'threshold_threads_connected') ?>

    <?php // echo $form->field($model, 'threshold_threads_running') ?>

    <?php // echo $form->field($model, 'threshold_threads_waits') ?>

    <?php // echo $form->field($model, 'threshold_repl_delay') ?>

    <?php // echo $form->field($model, 'slow_query') ?>

    <?php // echo $form->field($model, 'bigtable_monitor') ?>

    <?php // echo $form->field($model, 'bigtable_size') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
