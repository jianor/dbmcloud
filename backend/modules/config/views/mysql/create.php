<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MysqlServer */

$this->title = '新增MySQL实例';
$this->params['breadcrumbs'][] = ['label' => 'MySQL实例', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mysql-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
