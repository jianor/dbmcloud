<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\config\models\OracleServerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oracle实例配置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-server-index">

    <p>
        <?= Html::a('新增Oracle实例', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'tags',
            'host',
            'port',
            'dsn',
            'username',
            [
                'attribute' => 'monitor',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->monitor==1 ? Yii::t('app','ON') : Yii::t('app','OFF');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'monitor',
                    \common\models\Base::getOnStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '120'],

            ],
            [
                'attribute' => 'send_mail',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->send_mail==1 ? Yii::t('app','ON') : Yii::t('app','OFF');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'send_mail',
                    \common\models\Base::getOnStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '120'],

            ],
            [
                'attribute' => 'send_sms',
                'format' => 'html',
                'value' =>
                    function ($model) {
                        return $model->send_sms==1 ? Yii::t('app','ON') : Yii::t('app','OFF');
                    },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'send_sms',
                    \common\models\Base::getOnStatus(),
                    ['class' => 'form-control customer-select', 'prompt' => Yii::t('app', 'All')]
                ),
                'headerOptions' => ['width' => '120'],

            ],

            [
                'label'=>'',
                'format'=>'raw',
                'headerOptions' => ['width'=>'75'],
                'value' => function($model){
                    return
                        Html::a('<span class="glyphicon glyphicon-pencil"></span>',Yii::$app->getUrlManager()->createUrl(['config/oracle/update','id'=>$model['id']]), ['title' => Yii::t('app','Update') ]).'&nbsp&nbsp'
                        .Html::a('<span class="glyphicon glyphicon-trash"></span>',Yii::$app->getUrlManager()->createUrl(['config/oracle/delete','id'=>$model['id']]), ['title' => Yii::t('app','Delete') , 'data-method' => 'post','data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')] );
                }
            ],
        ],
    ]); ?>

</div>
