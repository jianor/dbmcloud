<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OracleServer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oracle Servers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oracle-server-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'host',
            'port',
            'dsn',
            'username',
            'password',
            'tags',
            'monitor',
            'send_mail',
            'send_mail_to_list',
            'send_sms',
            'send_sms_to_list',
            'alarm_session_total',
            'alarm_session_actives',
            'alarm_session_waits',
            'alarm_tablespace',
            'threshold_session_total',
            'threshold_session_actives',
            'threshold_session_waits',
            'threshold_tablespace',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
