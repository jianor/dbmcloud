<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OracleServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="oracle-server-form">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">连接信息</div>
                <div class="panel-body">
                    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'dsn')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'is_rac')->dropDownList((\common\models\Base::getYnStatus())) ?>


                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警通知</div>
                <div class="panel-body">

                    <?= $form->field($model, 'monitor')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_sms')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'send_mail_to_list')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'send_sms_to_list')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警项目</div>
                <div class="panel-body">

                    <?= $form->field($model, 'alarm_session_total')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_session_actives')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_session_waits')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_tablespace')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_objects')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_index')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_sqlmonitor')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_rollstat')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_temp_tbs')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_data_dict_rate')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_buffer_pool_rate')->dropDownList((\common\models\Base::getOnStatus())) ?>

                    <?= $form->field($model, 'alarm_table_hwm')->dropDownList((\common\models\Base::getOnStatus())) ?>

                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel panel-default">
                <div class="panel-heading">告警阀值</div>
                <div class="panel-body">

                    <?= $form->field($model, 'threshold_session_total')->textInput() ?>

                    <?= $form->field($model, 'threshold_session_actives')->textInput() ?>

                    <?= $form->field($model, 'threshold_session_waits')->textInput() ?>

                    <?= $form->field($model, 'threshold_tablespace')->textInput() ?>

                    <?= $form->field($model, 'threshold_objects')->textInput() ?>

                    <?= $form->field($model, 'threshold_index')->textInput() ?>

                    <?= $form->field($model, 'threshold_sqlmonitor')->textInput() ?>

                    <?= $form->field($model, 'threshold_rollstat')->textInput() ?>

                    <?= $form->field($model, 'threshold_temp_tbs')->textInput() ?>

                    <?= $form->field($model, 'threshold_data_dict_rate')->textInput() ?>

                    <?= $form->field($model, 'threshold_buffer_pool_rate')->textInput() ?>

                    <?= $form->field($model, 'threshold_table_hwm')->textInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? '创建' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('返回', ['index'], ['class' => 'btn btn-warning']) ?>
                    </div>

                </div>

            </div>

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
