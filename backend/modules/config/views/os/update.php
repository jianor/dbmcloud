<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OsServer */

$this->title = '更新主机SNMP: ' . ' ' . $model->tags;
$this->params['breadcrumbs'][] = ['label' => '主机', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tags, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';
?>
<div class="os-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
