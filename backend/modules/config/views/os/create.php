<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\OsServer */

$this->title = '新增主机SNMP';
$this->params['breadcrumbs'][] = ['label' => '主机', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="os-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
