<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MongodbServer */

$this->title = '更新Mongodb实例: ' . ' ' . $model->tags;
$this->params['breadcrumbs'][] = ['label' => 'Mongodb实例', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '更新';
?>
<div class="mongodb-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
