<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\config\models\MongodbServer */

$this->title = '新增Mongodb实例';
$this->params['breadcrumbs'][] = ['label' => 'Mongodb实例', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mongodb-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
