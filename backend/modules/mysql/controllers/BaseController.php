<?php
namespace backend\modules\mysql\controllers;

use Yii;
use yii\web\Controller;
use backend\controllers\BackendController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Base controller
 */
class BaseController extends BackendController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    public function init(){
        parent::init();
    }

}
