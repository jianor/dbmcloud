<?php

namespace backend\modules\mysql;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\mysql\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
