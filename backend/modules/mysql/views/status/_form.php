<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mysql-status-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'server_id')->textInput() ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'port')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'connect')->textInput() ?>

    <?= $form->field($model, 'role')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'uptime')->textInput() ?>

    <?= $form->field($model, 'version')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'max_connections')->textInput() ?>

    <?= $form->field($model, 'max_connect_errors')->textInput() ?>

    <?= $form->field($model, 'open_files_limit')->textInput() ?>

    <?= $form->field($model, 'open_files')->textInput() ?>

    <?= $form->field($model, 'table_open_cache')->textInput() ?>

    <?= $form->field($model, 'open_tables')->textInput() ?>

    <?= $form->field($model, 'max_tmp_tables')->textInput() ?>

    <?= $form->field($model, 'max_heap_table_size')->textInput() ?>

    <?= $form->field($model, 'max_allowed_packet')->textInput() ?>

    <?= $form->field($model, 'threads_connected')->textInput() ?>

    <?= $form->field($model, 'threads_running')->textInput() ?>

    <?= $form->field($model, 'threads_waits')->textInput() ?>

    <?= $form->field($model, 'threads_created')->textInput() ?>

    <?= $form->field($model, 'threads_cached')->textInput() ?>

    <?= $form->field($model, 'connections')->textInput() ?>

    <?= $form->field($model, 'aborted_clients')->textInput() ?>

    <?= $form->field($model, 'aborted_connects')->textInput() ?>

    <?= $form->field($model, 'connections_persecond')->textInput() ?>

    <?= $form->field($model, 'bytes_received_persecond')->textInput() ?>

    <?= $form->field($model, 'bytes_sent_persecond')->textInput() ?>

    <?= $form->field($model, 'com_select_persecond')->textInput() ?>

    <?= $form->field($model, 'com_insert_persecond')->textInput() ?>

    <?= $form->field($model, 'com_update_persecond')->textInput() ?>

    <?= $form->field($model, 'com_delete_persecond')->textInput() ?>

    <?= $form->field($model, 'com_commit_persecond')->textInput() ?>

    <?= $form->field($model, 'com_rollback_persecond')->textInput() ?>

    <?= $form->field($model, 'questions_persecond')->textInput() ?>

    <?= $form->field($model, 'queries_persecond')->textInput() ?>

    <?= $form->field($model, 'transaction_persecond')->textInput() ?>

    <?= $form->field($model, 'created_tmp_tables_persecond')->textInput() ?>

    <?= $form->field($model, 'created_tmp_disk_tables_persecond')->textInput() ?>

    <?= $form->field($model, 'created_tmp_files_persecond')->textInput() ?>

    <?= $form->field($model, 'table_locks_immediate_persecond')->textInput() ?>

    <?= $form->field($model, 'table_locks_waited_persecond')->textInput() ?>

    <?= $form->field($model, 'key_buffer_size')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'sort_buffer_size')->textInput() ?>

    <?= $form->field($model, 'join_buffer_size')->textInput() ?>

    <?= $form->field($model, 'key_blocks_not_flushed')->textInput() ?>

    <?= $form->field($model, 'key_blocks_unused')->textInput() ?>

    <?= $form->field($model, 'key_blocks_used')->textInput() ?>

    <?= $form->field($model, 'key_read_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'key_reads_persecond')->textInput() ?>

    <?= $form->field($model, 'key_write_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'key_writes_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_version')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'innodb_buffer_pool_instances')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_size')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'innodb_doublewrite')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'innodb_file_per_table')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'innodb_flush_log_at_trx_commit')->textInput() ?>

    <?= $form->field($model, 'innodb_flush_method')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'innodb_force_recovery')->textInput() ?>

    <?= $form->field($model, 'innodb_io_capacity')->textInput() ?>

    <?= $form->field($model, 'innodb_read_io_threads')->textInput() ?>

    <?= $form->field($model, 'innodb_write_io_threads')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_total')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_data')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_dirty')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_flushed')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_free')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_misc')->textInput() ?>

    <?= $form->field($model, 'innodb_page_size')->textInput() ?>

    <?= $form->field($model, 'innodb_pages_created')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'innodb_pages_read')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'innodb_pages_written')->textInput(['maxlength' => 18]) ?>

    <?= $form->field($model, 'innodb_row_lock_current_waits')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'innodb_buffer_pool_pages_flushed_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_read_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_reads_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_buffer_pool_write_requests_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_rows_read_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_rows_inserted_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_rows_updated_persecond')->textInput() ?>

    <?= $form->field($model, 'innodb_rows_deleted_persecond')->textInput() ?>

    <?= $form->field($model, 'query_cache_hitrate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'thread_cache_hitrate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'key_buffer_read_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'key_buffer_write_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'key_blocks_used_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'created_tmp_disk_tables_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'connections_usage_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'open_files_usage_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'open_tables_usage_rate')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
