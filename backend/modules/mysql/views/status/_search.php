<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mysql-status-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'server_id') ?>

    <?= $form->field($model, 'host') ?>

    <?= $form->field($model, 'port') ?>

    <?= $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'connect') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'uptime') ?>

    <?php // echo $form->field($model, 'version') ?>

    <?php // echo $form->field($model, 'max_connections') ?>

    <?php // echo $form->field($model, 'max_connect_errors') ?>

    <?php // echo $form->field($model, 'open_files_limit') ?>

    <?php // echo $form->field($model, 'open_files') ?>

    <?php // echo $form->field($model, 'table_open_cache') ?>

    <?php // echo $form->field($model, 'open_tables') ?>

    <?php // echo $form->field($model, 'max_tmp_tables') ?>

    <?php // echo $form->field($model, 'max_heap_table_size') ?>

    <?php // echo $form->field($model, 'max_allowed_packet') ?>

    <?php // echo $form->field($model, 'threads_connected') ?>

    <?php // echo $form->field($model, 'threads_running') ?>

    <?php // echo $form->field($model, 'threads_waits') ?>

    <?php // echo $form->field($model, 'threads_created') ?>

    <?php // echo $form->field($model, 'threads_cached') ?>

    <?php // echo $form->field($model, 'connections') ?>

    <?php // echo $form->field($model, 'aborted_clients') ?>

    <?php // echo $form->field($model, 'aborted_connects') ?>

    <?php // echo $form->field($model, 'connections_persecond') ?>

    <?php // echo $form->field($model, 'bytes_received_persecond') ?>

    <?php // echo $form->field($model, 'bytes_sent_persecond') ?>

    <?php // echo $form->field($model, 'com_select_persecond') ?>

    <?php // echo $form->field($model, 'com_insert_persecond') ?>

    <?php // echo $form->field($model, 'com_update_persecond') ?>

    <?php // echo $form->field($model, 'com_delete_persecond') ?>

    <?php // echo $form->field($model, 'com_commit_persecond') ?>

    <?php // echo $form->field($model, 'com_rollback_persecond') ?>

    <?php // echo $form->field($model, 'questions_persecond') ?>

    <?php // echo $form->field($model, 'queries_persecond') ?>

    <?php // echo $form->field($model, 'transaction_persecond') ?>

    <?php // echo $form->field($model, 'created_tmp_tables_persecond') ?>

    <?php // echo $form->field($model, 'created_tmp_disk_tables_persecond') ?>

    <?php // echo $form->field($model, 'created_tmp_files_persecond') ?>

    <?php // echo $form->field($model, 'table_locks_immediate_persecond') ?>

    <?php // echo $form->field($model, 'table_locks_waited_persecond') ?>

    <?php // echo $form->field($model, 'key_buffer_size') ?>

    <?php // echo $form->field($model, 'sort_buffer_size') ?>

    <?php // echo $form->field($model, 'join_buffer_size') ?>

    <?php // echo $form->field($model, 'key_blocks_not_flushed') ?>

    <?php // echo $form->field($model, 'key_blocks_unused') ?>

    <?php // echo $form->field($model, 'key_blocks_used') ?>

    <?php // echo $form->field($model, 'key_read_requests_persecond') ?>

    <?php // echo $form->field($model, 'key_reads_persecond') ?>

    <?php // echo $form->field($model, 'key_write_requests_persecond') ?>

    <?php // echo $form->field($model, 'key_writes_persecond') ?>

    <?php // echo $form->field($model, 'innodb_version') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_instances') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_size') ?>

    <?php // echo $form->field($model, 'innodb_doublewrite') ?>

    <?php // echo $form->field($model, 'innodb_file_per_table') ?>

    <?php // echo $form->field($model, 'innodb_flush_log_at_trx_commit') ?>

    <?php // echo $form->field($model, 'innodb_flush_method') ?>

    <?php // echo $form->field($model, 'innodb_force_recovery') ?>

    <?php // echo $form->field($model, 'innodb_io_capacity') ?>

    <?php // echo $form->field($model, 'innodb_read_io_threads') ?>

    <?php // echo $form->field($model, 'innodb_write_io_threads') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_total') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_data') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_dirty') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_flushed') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_free') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_misc') ?>

    <?php // echo $form->field($model, 'innodb_page_size') ?>

    <?php // echo $form->field($model, 'innodb_pages_created') ?>

    <?php // echo $form->field($model, 'innodb_pages_read') ?>

    <?php // echo $form->field($model, 'innodb_pages_written') ?>

    <?php // echo $form->field($model, 'innodb_row_lock_current_waits') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_pages_flushed_persecond') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_read_requests_persecond') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_reads_persecond') ?>

    <?php // echo $form->field($model, 'innodb_buffer_pool_write_requests_persecond') ?>

    <?php // echo $form->field($model, 'innodb_rows_read_persecond') ?>

    <?php // echo $form->field($model, 'innodb_rows_inserted_persecond') ?>

    <?php // echo $form->field($model, 'innodb_rows_updated_persecond') ?>

    <?php // echo $form->field($model, 'innodb_rows_deleted_persecond') ?>

    <?php // echo $form->field($model, 'query_cache_hitrate') ?>

    <?php // echo $form->field($model, 'thread_cache_hitrate') ?>

    <?php // echo $form->field($model, 'key_buffer_read_rate') ?>

    <?php // echo $form->field($model, 'key_buffer_write_rate') ?>

    <?php // echo $form->field($model, 'key_blocks_used_rate') ?>

    <?php // echo $form->field($model, 'created_tmp_disk_tables_rate') ?>

    <?php // echo $form->field($model, 'connections_usage_rate') ?>

    <?php // echo $form->field($model, 'open_files_usage_rate') ?>

    <?php // echo $form->field($model, 'open_tables_usage_rate') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
