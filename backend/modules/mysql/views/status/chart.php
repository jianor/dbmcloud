<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\widgets\DetailView;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags . ':' . $model->role . ']';
$this->params['breadcrumbs'][] = ['label' => 'MySQL 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;
?>

<?php
$data = $model->find()->orderBy(['id' => SORT_DESC, 'host' => SORT_ASC])->all();
echo $this->render('_tab', ['model' => $model]);
?>

<div class="container-fluid mt15">
    <div class="col-xs-5 text-left">
        <a href="<?= Yii::$app->urlManager->createUrl(['mysql/status/chart/', 'server' => $model->host . '-' . $model->port, 'time_interval' => '3600', 'end' => '0']); ?>"
           class="btn <?php if ($time_interval == 3600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['mysql/status/chart/', 'server' => $model->host . '-' . $model->port, 'time_interval' => '21600', 'end' => '0']); ?>"
           class="btn <?php if ($time_interval == 21600): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">6小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['mysql/status/chart/', 'server' => $model->host . '-' . $model->port, 'time_interval' => '43200', 'end' => '0']); ?>"
           class="btn <?php if ($time_interval == 43200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">12小时</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['mysql/status/chart/', 'server' => $model->host . '-' . $model->port, 'time_interval' => '86400', 'end' => '0']); ?>"
           class="btn <?php if ($time_interval == 86400): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">1天</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['mysql/status/chart/', 'server' => $model->host . '-' . $model->port, 'time_interval' => '259200', 'end' => '0']); ?>"
           class="btn <?php if ($time_interval == 259200): ?> btn-danger <?php else: ?> btn-primary <?php endif; ?>">3天</a>
    </div>
    <?php $form = ActiveForm::begin(['action' => ['status/chart'], 'method' => 'post']); ?>
    <div class="col-xs-5 text-left">

        <?= DateRangePicker::widget([
            'name' => 'time_from',
            'value' => $time_from,
            'nameTo' => 'time_to',
            'valueTo' => $time_to,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-m-d'
            ]
        ]); ?>

    </div>
    <div class="col-xs-1 text-left">
        <?= Html::activeHiddenInput($model, 'host'); ?>
        <?= Html::activeHiddenInput($model, 'port'); ?>
        <?= Html::submitButton('生成图表', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<hr/>
<div class="container-fluid mt15">

    <a href="#main_chart_connect" class="btn btn-warning btn-xs">连接状态</a>
    <a href="#main_chart_running" class="btn btn-warning btn-xs">Threads Running</a>
    <a href="#main_chart_waits" class="btn btn-warning btn-xs">Threads Waits</a>
    <a href="#main_chart_2" class="btn btn-warning btn-xs">QPS/TPS</a>
    <a href="#main_chart_3" class="btn btn-warning btn-xs">DML</a>
    <a href="#main_chart_4" class="btn btn-warning btn-xs">事物</a>
    <a href="#main_chart_5" class="btn btn-warning btn-xs">InnoDB读</a>
    <a href="#main_chart_6" class="btn btn-warning btn-xs">InnoDB写</a>
    <a href="#main_chart_7" class="btn btn-warning btn-xs">DB网络</a>
    <a href="#main_chart_load" class="btn btn-warning btn-xs">CPU Load</a>
    <a href="#main_chart_cpu" class="btn btn-warning btn-xs">CPU Time</a>

    <hr/>
    <!--<div id="main_chart_test" class="col-xs-12 text-center line-chart"></div>-->
    <div id="main_chart_connect" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_running" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_waits" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_2" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_3" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_4" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_5" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_6" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_7" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_cpu" class="col-xs-12 text-center line-chart"></div>
    <div id="main_chart_load" class="col-xs-12 text-center line-chart"></div>

</div>



<?php \common\widgets\JsBlock::begin() ?>

<script type="text/javascript">
    //连接状态
    var myChart = echarts.init(document.getElementById('main_chart_connect'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color: ['#66CC99'],
        title: {
            text: '连接状态',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['连接状态'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: '连接状态',
                type: 'line',
                smooth: true,
                itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['connect'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //threads running
    var myChart = echarts.init(document.getElementById('main_chart_running'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title: {
            text: 'Threads Running',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Threads Running', 'Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Threads Running',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['threads_running'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_threads_running'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //threads waits
    var myChart = echarts.init(document.getElementById('main_chart_waits'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title: {
            text: 'Threads Waits',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Threads Waits', 'Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Threads Waits',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['threads_waits'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item->servers['threshold_threads_waits'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //QPS/TPS
    var myChart = echarts.init(document.getElementById('main_chart_2'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color: ['#009933', '#FF3333'],
        title: {
            text: '数据库QPS/TPS',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['QPS', 'TPS'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'QPS',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['queries_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'TPS',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['transaction_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">
    //DML
    var myChart = echarts.init(document.getElementById('main_chart_3'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color: ['#009933', '#FF3333', '#FF9933'],
        title: {
            text: '数据库DML语句',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Insert', 'Update', 'Delete'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Insert',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['com_insert_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'Update',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['com_update_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

            {
                name: 'Delete',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['com_delete_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });
</script>

<script type="text/javascript">
    //事物
    var myChart = echarts.init(document.getElementById('main_chart_4'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color: ['#009933', '#FF3333'],
        title: {
            text: '数据库事务',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Commit', 'Rollback'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Commit',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['com_commit_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'Rollback',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['com_rollback_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });
</script>

<script type="text/javascript">
    //InnoDB读
    var myChart = echarts.init(document.getElementById('main_chart_5'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color: ['#009933', '#FF3333'],
        title: {
            text: 'InnoDB Read',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Rows Select'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Rows Select',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['innodb_rows_read_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });
</script>

<script type="text/javascript">
    //InnoDB写
    var myChart = echarts.init(document.getElementById('main_chart_6'));
    myChart.setOption({
        backgroundColor: '#FFF',
        color: ['#009933', '#FF3333', '#FF9933'],
        title: {
            text: 'InnoDB Write Rows',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Rows Insert', 'Rows Update', 'Rows Delete'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Rows Insert',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['innodb_rows_inserted_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'Rows Update',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['innodb_rows_updated_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'Rows Delete',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['innodb_rows_deleted_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });
</script>

<script type="text/javascript">
    //网络
    var myChart = echarts.init(document.getElementById('main_chart_7'));
    myChart.setOption({
        backgroundColor: '#FFF',
        color: ['#009933', '#FF3333'],
        title: {
            text: '数据库网络流量',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                //color: '#FFFFFF'
            }
        },
        legend: {
            data: ['Receive', 'Sent'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip: {
            trigger: 'axis'
        },

        toolbox: {
            show: true,
            feature: {
                mark: {show: false},
                dataView: {show: true, readOnly: true},
                magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: false,
        animation: true,
        dataZoom: {
            show: true,
            realtime: true,
            start: 0,
            end: 100
        },
        xAxis: [
            {
                type: 'category',
                boundaryGap: false,
                axisLine: {onZero: false},
                data: [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis: [
            {
                name: '流量(KB)',
                type: 'value'
            }
        ],
        series: [
            {
                name: 'Receive',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['bytes_received_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name: 'Sent',
                type: 'line',
                smooth: true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data: [
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['bytes_sent_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });
</script>

<?php
    $_chart_data_array = array(
        0=>array(
            'name'=>'邮件营销','data'=>array(10,20,30,40,50,60,70)
        ),
        1=>array(
            'name'=>'联盟广告','data'=>array(80,70,60,50,40,30,20)
        ),
        2=>array(
            'name'=>'视频广告','data'=>array(10,10,10,10,10,10,10)
        ),
    );


?>

<script type="text/javascript">
    //test
    var myChart = echarts.init(document.getElementById('main_chart_test'));

    myChart.setOption({
        tooltip: {
            trigger: 'axis',
            //formatter: '{b0}<br />{a0}: {c0} {c0} <br />{a1}: {c1}%  <br />{a2}: {c2}% ',
            axisPointer: { // 坐标轴指示器，坐标轴触发有效
                type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
            }
        },
        legend: {
            data: ['邮件营销', '联盟广告', '视频广告']
        },
        toolbox: {
            show: true,
            orient: 'vertical',
            x: 'right',
            y: 'center',
            feature: {
                mark: true,
                dataView: {readOnly: false},
                magicType: ['line', 'bar'],
                restore: true,
                saveAsImage: true
            }
        },
        calculable: true,
        xAxis: [
            {
                type: 'category',
                data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLabel: {
                    show: true,
                    interval: 'auto',
                    formatter: '{value} %'
                },
                splitArea: {show: true}
            }
        ],
        series: [
            <?php foreach($_chart_data_array as $key=>$value){ ?>
            {
                name: '<?php echo $value['name'];?>',
                type: 'bar',
                stack: '广告',
                data: [ <?php echo implode(',',$value['data']); ?> ]
            },
            <?php } ?>
        ]


    })
    ;
</script>




<script type="text/javascript">
    //load
    var myChart = echarts.init(document.getElementById('main_chart_load'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title : {
            text: 'CPU Load',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['CPU Load','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($os_chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'CPU Load',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item['load_1'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },

            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item->servers['threshold_os_load'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });

</script>

<script type="text/javascript">
    //cpu
    var myChart = echarts.init(document.getElementById('main_chart_cpu'));
    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#009933', '#FF3333','#FF9966'],
        title : {
            text: 'CPU Utilization',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder'
                //color: '#FFFFFF'
            }
        },
        legend: {
            data:['CPU Time Usage','Alarm Threshold'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '60px',
            x2: '60px',
            y: '35px',
            y2: '60px'
        },
        tooltip : {
            trigger: 'axis',
            enterable:true
            //formatter:'{b1} <br /> {a0}: {c0}<br />{a1}: {c1}<br /><a href="">Detail</a> '
        },

        toolbox: {
            show : true,
            feature : {
                mark : {show: false},
                dataView : {show: true, readOnly: true},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : false,
        animation : true,
        dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
        },
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($os_chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$date $time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value',
                max:'100',
                axisLabel: {
                    show: true,
                    interval: 'auto',
                    formatter: '{value}%'
                }
            }
        ],
        series : [
            {
                name:'CPU Time Usage',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item['cpu_user_time']+$item['cpu_system_time'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ],
                markLine: {
                    data: [
                        {type: 'average', name: '平均值'}
                    ]
                }
            },

            {
                name:'Alarm Threshold',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($os_chart_data as $item):
                            $data_array[] = $item->servers['threshold_os_cpu'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
        ]
    });

</script>

<?php \common\widgets\JsBlock::end() ?>


