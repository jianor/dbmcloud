<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use backend\helpers\Tools;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->role. ']';
$this->params['breadcrumbs'][] = ['label' => 'MySQL 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host.':'.$model->port;

?>

<?php
$data = $model->find()->orderBy(['id' => SORT_DESC, 'host' => SORT_ASC])->all();
echo $this->render('_tab',['model'=>$model]);
?>

<div class="container-fluid mt15">



    <div class="col-xs-12 text-left">

        <div class="progress  active">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                <span class="">数据库性能评分：90分</span>
            </div>
        </div>

        <table class="table table-bordered table-hover table-striped" style="margin-top: 15px;">
            <thead>
            <tr>
                <th width="10%">类型</th>
                <th width="10%">指标</th>
                <th width="6%">当前值</th>
                <th width="10%">参考值</th>
                <th width="8%">体检结果</th>
                <th>备注/优化建议</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td rowspan="5" class="text-center"><b>线程池</b></td>
                <td>max_connections</td>
                <td><?= $model->max_connections ?></td>
                <td>>=1</td>
                <td>变量值</td>
                <td>连接到MySQL数据库的连接数最大值限制</td>
            </tr>
            <tr>
                <td>threads_connected</td>
                <td><?= $model->threads_connected ?></td>
                <td>>=1</td>
                <td>状态值</td>
                <td>客户端连接到MySQL数据库的连接总数 该数值不能超过 max_connections 设置的值</td>
            </tr>
            <tr>
                <td>threads_running</td>
                <td><?= $model->threads_running ?></td>
                <td>0-10</td>
                <td><?php if($model->threads_running>10): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>MySQL数据库当前正在运行SQL的活动进程数量(非Sleep线程)</td>
            </tr>
            <tr>
                <td>threads_waits</td>
                <td><?= $model->threads_waits ?></td>
                <td>0-3</td>
                <td><?php if($model->threads_waits>3): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>当前等待的SQL进程，如果该指标非0，请查看数据库是否有锁或者长时间未执行完毕的SQL语句</td>
            </tr>
            <tr>
                <td>连接池使用占比</td>
                <td><?= $model->connections_usage_rate*100 ?>%</td>
                <td><80%</td>
                <td><?php if($model->connections_usage_rate*100>=80): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>连接池使用率，当连接池使用满后，将造成应用连接错误。如果该值较大，需要增加  max_connections 的值</td>
            </tr>

            <tr>
                <td rowspan="3" class="text-center"><b>文件</b></td>
                <td>open_files_limit</td>
                <td><?= $model->open_files_limit ?></td>
                <td>1024-65536</td>
                <td>变量值</td>
                <td>MySQL数据库允许打开文件的最大值限制 </td>
            </tr>
            <tr>
                <td>open_files</td>
                <td><?= $model->open_files ?></td>
                <td>>1</td>
                <td>状态值</td>
                <td>MySQL数据库文件打开的数量,不包括诸如套接字或管道其他类型的文件。 请勿超过 open_files_limit 的值 </td>
            </tr>

            <tr>
                <td>打开文件占比</td>
                <td><?= $model->open_files_usage_rate*100 ?>%</td>
                <td><60%</td>
                <td><?php if($model->open_files_usage_rate*100>=60): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>(open_files/open_files_limit)*100% ,如果超过最大文件打开数则会引起数据库运行异常。如果该占比值较大，需要增加 open_files_limit 的值 </td>
            </tr>

            <tr>
                <td rowspan="3" class="text-center"><b>表缓存</b></td>
                <td>table_open_cache</td>
                <td><?= $model->table_open_cache ?></td>
                <td>128-4096</td>
                <td>变量值</td>
                <td>设置表高速缓存的数目，每个连接进来，都会至少打开一个表缓存。因此， table_cache 的大小应与 max_connections 的设置有关。</td>
            </tr>
            <tr>
                <td>open_tables</td>
                <td><?= $model->open_tables ?></td>
                <td>>1</td>
                <td>状态值</td>
                <td>当前打开的表的数量</td>
            </tr>
            <tr>
                <td>表缓存使用率</td>
                <td><?= $model->open_tables_usage_rate*100 ?>%</td>
                <td><90%</td>
                <td><?php if($model->open_tables_usage_rate*100>=90): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>如果该比率较大，需要增加 table_open_cache 的值 </td>
            </tr>

            <tr>
                <td rowspan="5" class="text-center"><b>临时表</b></td>
                <td>tmp_table_size</td>
                <td><?= Tools::formatBytes($model->tmp_table_size); ?></td>
                <td>16MB-256MB</td>
                <td>变量值</td>
                <td>内存中的临时表大小，超过该大小会转换成MyISAM表，增大该值有益于用到临时表的操作，比如group by（实际起限制作用的是tmp_table_size和max_heap_table_size的最小值）</td>
            </tr>
            <tr>
                <td>max_heap_table_size</td>
                <td><?= Tools::formatBytes($model->max_heap_table_size); ?></td>
                <td>16MB-256MB</td>
                <td>变量值</td>
                <td>Heap(Memroy)表最大尺寸，达到该大小时，MySQL会返回错误，该值大于tmp_table_size无用</td>
            </tr>
            <tr>
                <td>created_tmp_tables</td>
                <td><?= $model->created_tmp_tables ?></td>
                <td>>0</td>
                <td>状态值</td>
                <td>服务器执行语句时自动创建的内存中的临时表的数量</td>
            </tr>
            <tr>
                <td>created_tmp_disk_tables</td>
                <td><?= $model->created_tmp_disk_tables ?></td>
                <td>>0</td>
                <td>状态值</td>
                <td>服务器执行语句时自动创建的磁盘中的临时表的数量</td>
            </tr>
            <tr>
                <td>硬盘临时表占比</td>
                <td><?= $model->created_tmp_disk_tables_rate*100 ?>%</td>
                <td><10%</td>
                <td><?php if($model->created_tmp_disk_tables_rate*100>10): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>服务器执行语句时在硬盘上自动创建的临时表数量占所有临时表(磁盘+内存)的比率 如果该值较大，需要增加 tmp_table_size 的值使临时表基于内存而不基于硬盘 </td>
            </tr>

            <tr>
                <td rowspan="8" class="text-center"><b>索引缓冲区</b></td>
                <td>key_buffer_size</td>
                <td><?= Tools::formatBytes($model->key_buffer_size); ?></td>
                <td>16M-2GB</td>
                <td>变量值</td>
                <td></td>
            </tr>
            <tr>
                <td>sort_buffer_size</td>
                <td><?= Tools::formatBytes($model->sort_buffer_size); ?></td>
                <td>64KB-4MB</td>
                <td>变量值</td>
                <td>排序缓存，当结果集需要排序，如order/group by操作，但又不能使用索引排序时，会使用到该缓存</td>
            </tr>
            <tr>
                <td>join_buffer_size</td>
                <td><?= Tools::formatBytes($model->join_buffer_size); ?></td>
                <td>64KB-4MB</td>
                <td>变量值</td>
                <td>当表之间的join不能使用索引时，会使用到该缓存，按表个数分配</td>
            </tr>
            <tr>
                <td>read_buffer_size</td>
                <td><?= Tools::formatBytes($model->read_buffer_size); ?></td>
                <td>64KB-4MB</td>
                <td>变量值</td>
                <td>顺序读缓存，数据在磁盘上物理连续，按表个数分配</td>
            </tr>
            <tr>
                <td>read_rnd_buffer_size</td>
                <td><?= Tools::formatBytes($model->read_rnd_buffer_size); ?></td>
                <td>64KB-4MB</td>
                <td>变量值</td>
                <td>走索引的随机读缓存，数据在索引中连续，在磁盘上物理随机</td>
            </tr>
            <tr>
                <td>key_blocks_used_rate</td>
                <td><?= $model->key_blocks_used_rate*100 ?>%</td>
                <td><80%</td>
                <td><?php if($model->key_blocks_used_rate*100>=90): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>连接池使用率，当连接池使用满后，将造成应用连接错误。如果该值较大，需要增加  max_connections 的值</td>
            </tr>
            <tr>
                <td>key_buffer_read_rate</td>
                <td><?= $model->key_buffer_read_rate*100 ?>%</td>
                <td><80%</td>
                <td><?php if($model->key_buffer_read_rate*100>=90): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>连接池使用率，当连接池使用满后，将造成应用连接错误。如果该值较大，需要增加  max_connections 的值</td>
            </tr>
            <tr>
                <td>key_buffer_write_rate</td>
                <td><?= $model->key_buffer_write_rate*100 ?>%</td>
                <td><80%</td>
                <td><?php if($model->key_buffer_write_rate*100>=90): ?><span class='btn btn-danger btn-xs'>-5</span><?php else: ?><span class='btn btn-success btn-xs'>OK</span><?php endif; ?></td>
                <td>连接池使用率，当连接池使用满后，将造成应用连接错误。如果该值较大，需要增加  max_connections 的值</td>
            </tr>


            </tbody>
        </table>
    </div>

</div>



<?php \common\widgets\JsBlock::begin() ?>
<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart'));

    myChart.setOption({
        backgroundColor: '#FFFFFF',
        color:['#DD4B39','#00A65A'],
        title : {
            text: 'Key Block使用率',
            subtext: '连接使用率超过80%请增加',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'vertical',
            x : 'left',
            data:['已用 Block','空闲 Block']
        },
        toolbox: {
            show : true,
            feature : {
                //mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        series : [
            {
                name:'key block使用率',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:<?=$model->key_blocks_used; ?>, name:'已用 Block'},
                    {value:<?=($model->key_blocks_unused); ?>, name:'空闲 Block'},
                ]
            }
        ]
    });

</script>

<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart2'));

    myChart.setOption({
        color:['#DD4B39','#00A65A'],
        title : {
            text: '文件句柄资源',
            subtext: '文件句柄使用率超过60%请增加',
            x:'center'
        },
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'vertical',
            x : 'left',
            data:['已用文件句柄','可用文件句柄']
        },
        toolbox: {
            show : true,
            feature : {
                //mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {
                    show: true,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            x: '25%',
                            width: '50%',
                            funnelAlign: 'left',
                            max: 1548
                        }
                    }
                },
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        series : [
            {
                name:'文件句柄',
                type:'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:<?=$model->open_files; ?>, name:'已用文件句柄'},
                    {value:<?=($model->open_files_limit-$model->open_files); ?>, name:'可用文件句柄'},
                ]
            }
        ]
    });

</script>
<?php \common\widgets\JsBlock::end()?>


