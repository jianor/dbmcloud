<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use backend\helpers\Tools;

/* @var $this yii\web\View */
/* @var $model backend\models\MysqlStatus */

$this->title = $model->host . ':' . $model->port . ' [' . $model->tags .':'.$model->role. ']';
$this->params['breadcrumbs'][] = ['label' => 'MySQL 性能监控平台', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->host . ':' . $model->port;

?>

<?php echo $this->render('_tab', ['model' => $model]); ?>

<div class="container-fluid mt5">
    <div class="col-xs-8 text-left">
        <table class="table table-bordered table-hover table-striped" style="margin-top: 15px;">
            <thead>
            <tr>
                <th>指标</th>
                <th>数值</th>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>实例名称</td>
                <td><?= $model->host . ':' . $model->port ?></td>
                <td>当前数据库的连接主机和端口信息</td>
            </tr>
            <tr>
                <td>采集时间</td>
                <td><?= $model->create_time ?></td>
                <td>采集进程最后执行采集任务的时间</td>
            </tr>
            <tr>
                <td>实例状态</td>
                <td><span class="btn btn-success btn-xs">运行</span></td>
                <td>当前数据库是否正常运行</td>
            </tr>
            <tr>
                <td>运行时间</td>
                <td><?= Tools::checkUptime($model->uptime); ?></td>
                <td>当前数据库自启动以来运行时间</td>
            </tr>
            <tr>
                <td>版本号</td>
                <td><?= $model->version ?></td>
                <td>当前数据库运行版本</td>
            </tr>
            <tr>
                <td>数据库角色</td>
                <td><?= $model->role ?></td>
                <td>主库或者备库</td>
            </tr>
            <tr>
                <td>连接池使用率</td>
                <td><?= $model->connections_usage_rate*100 ?>%</td>
                <td>连接池使用率 参考值：60%</td>
            </tr>
            <tr>
                <td>运行SQL进程数</td>
                <td><?= $model->threads_running; ?></td>
                <td>当前数据库正在运行的SQL进程数 参考值:<=30</td>
            </tr>
            <tr>
                <td>等待SQL进程数</td>
                <td><?= $model->threads_waits; ?></td>
                <td>进程中SQL等待数 参考值：<=5</td>
            </tr>
            <tr>
                <td>每秒查询数(QPS)</td>
                <td><?= $model->queries_persecond; ?></td>
                <td>数据库每秒查询数量</td>
            </tr>
            <tr>
                <td>每秒事物数(TPS)</td>
                <td><?= $model->transaction_persecond; ?></td>
                <td>数据库每秒事物数量</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-xs-4 text-center ">
        <div id="main_chart1" class="line-chart-x4 mt5"></div>
        <div id="main_chart2" class="line-chart-x4 mt5"></div>
    </div>
</div>


<?php \common\widgets\JsBlock::begin() ?>
<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart1'));

    myChart.setOption({
        //backgroundColor: '#FFF',
        color:['#FF9966','#FF0033'],
        title : {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                color: '#FFFFFF'
            }
        },
        legend: {
            data:['SQL运行线程数','SQL等待线程数'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '40px',
            x2: '20px',
            y: '30px',
            y2: '30px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : false,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'SQL运行线程数',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['threads_running'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'SQL等待线程数',
                type:'line',
                smooth:true,
                //itemStyle: {normal: {areaStyle: {type: 'default'}}},
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['threads_waits'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<script type="text/javascript">

    var myChart = echarts.init(document.getElementById('main_chart2'));

    myChart.setOption({
        //backgroundColor: '#FFF',

        title : {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                fontSize: 14,
                fontWeight: 'bolder',
                color: '#FFFFFF'
            }
        },
        legend: {
            data:['QPS/每秒查询数','TPS/每秒事务数'],
            x: 'left',
            textStyle: {
                fontSize: 8,
                color: '#333333'
            }
        },
        grid: {
            x: '40px',
            x2: '20px',
            y: '30px',
            y2: '30px'
        },
        tooltip : {
            trigger: 'axis'
        },

        toolbox: {
            show : false,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        xAxis : [
            {
                type : 'category',
                boundaryGap : false,
                axisLine: {onZero: false},
                data : [
                    <?php
                        $time_array=array();
                        foreach($chart_data as $item):
                            $date = date('m/d',strtotime($item['create_time']));
                            $time = date('H:i',strtotime($item['create_time']));
                            $time_array[]="'$time'";
                        endforeach;
                        echo implode(',',$time_array);
                     ?>
                ]
            }
        ],
        yAxis : [
            {
                type : 'value'
            }
        ],
        series : [
            {
                name:'QPS/每秒查询数',
                type:'line',
                smooth:true,
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['queries_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },
            {
                name:'TPS/每秒事务数',
                type:'line',
                smooth:true,
                data:[
                    <?php
                        $data_array=array();
                        foreach($chart_data as $item):
                            $data_array[] = $item['transaction_persecond'];
                        endforeach;
                        echo implode(',',$data_array);
                     ?>
                ]
            },

        ]
    });

</script>

<?php \common\widgets\JsBlock::end()?>



