<?php

namespace backend\modules\mysql\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "mysql_status".
 *
 * @property integer $id
 * @property string $host
 * @property string $port
 * @property string $tags
 * @property integer $connect
 * @property string $role
 * @property integer $uptime
 * @property string $version
 * @property integer $max_connections
 * @property integer $max_connect_errors
 * @property integer $open_files_limit
 * @property integer $open_files
 * @property integer $table_open_cache
 * @property integer $open_tables
 * @property integer $max_tmp_tables
 * @property integer $max_heap_table_size
 * @property integer $max_allowed_packet
 * @property integer $threads_connected
 * @property integer $threads_running
 * @property integer $threads_waits
 * @property integer $threads_created
 * @property integer $threads_cached
 * @property integer $connections
 * @property integer $aborted_clients
 * @property integer $aborted_connects
 * @property integer $connections_persecond
 * @property integer $bytes_received_persecond
 * @property integer $bytes_sent_persecond
 * @property integer $com_select_persecond
 * @property integer $com_insert_persecond
 * @property integer $com_update_persecond
 * @property integer $com_delete_persecond
 * @property integer $com_commit_persecond
 * @property integer $com_rollback_persecond
 * @property integer $questions_persecond
 * @property integer $queries_persecond
 * @property integer $transaction_persecond
 * @property integer $created_tmp_tables_persecond
 * @property integer $created_tmp_disk_tables_persecond
 * @property integer $created_tmp_files_persecond
 * @property integer $table_locks_immediate_persecond
 * @property integer $table_locks_waited_persecond
 * @property string $key_buffer_size
 * @property integer $sort_buffer_size
 * @property integer $join_buffer_size
 * @property integer $key_blocks_not_flushed
 * @property integer $key_blocks_unused
 * @property integer $key_blocks_used
 * @property integer $key_read_requests_persecond
 * @property integer $key_reads_persecond
 * @property integer $key_write_requests_persecond
 * @property integer $key_writes_persecond
 * @property string $innodb_version
 * @property integer $innodb_buffer_pool_instances
 * @property string $innodb_buffer_pool_size
 * @property string $innodb_doublewrite
 * @property string $innodb_file_per_table
 * @property integer $innodb_flush_log_at_trx_commit
 * @property string $innodb_flush_method
 * @property integer $innodb_force_recovery
 * @property integer $innodb_io_capacity
 * @property integer $innodb_read_io_threads
 * @property integer $innodb_write_io_threads
 * @property integer $innodb_buffer_pool_pages_total
 * @property integer $innodb_buffer_pool_pages_data
 * @property integer $innodb_buffer_pool_pages_dirty
 * @property string $innodb_buffer_pool_pages_flushed
 * @property integer $innodb_buffer_pool_pages_free
 * @property integer $innodb_buffer_pool_pages_misc
 * @property integer $innodb_page_size
 * @property string $innodb_pages_created
 * @property string $innodb_pages_read
 * @property string $innodb_pages_written
 * @property string $innodb_row_lock_current_waits
 * @property integer $innodb_buffer_pool_pages_flushed_persecond
 * @property integer $innodb_buffer_pool_read_requests_persecond
 * @property integer $innodb_buffer_pool_reads_persecond
 * @property integer $innodb_buffer_pool_write_requests_persecond
 * @property integer $innodb_rows_read_persecond
 * @property integer $innodb_rows_inserted_persecond
 * @property integer $innodb_rows_updated_persecond
 * @property integer $innodb_rows_deleted_persecond
 * @property string $query_cache_hitrate
 * @property string $thread_cache_hitrate
 * @property string $key_buffer_read_rate
 * @property string $key_buffer_write_rate
 * @property string $key_blocks_used_rate
 * @property string $created_tmp_disk_tables_rate
 * @property string $connections_usage_rate
 * @property string $open_files_usage_rate
 * @property string $open_tables_usage_rate
 * @property string $create_time
 */
class MysqlStatus extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'mysql_status';
    }

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            // BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['connect', 'uptime', 'max_connections', 'max_connect_errors', 'open_files_limit', 'open_files', 'table_open_cache', 'open_tables', 'max_tmp_tables', 'max_heap_table_size', 'max_allowed_packet', 'threads_connected', 'threads_running', 'threads_waits', 'threads_created', 'threads_cached', 'connections', 'aborted_clients', 'aborted_connects', 'connections_persecond', 'bytes_received_persecond', 'bytes_sent_persecond', 'com_select_persecond', 'com_insert_persecond', 'com_update_persecond', 'com_delete_persecond', 'com_commit_persecond', 'com_rollback_persecond', 'questions_persecond', 'queries_persecond', 'transaction_persecond', 'created_tmp_tables_persecond', 'created_tmp_disk_tables_persecond', 'created_tmp_files_persecond', 'table_locks_immediate_persecond', 'table_locks_waited_persecond', 'key_buffer_size', 'sort_buffer_size', 'join_buffer_size', 'key_blocks_not_flushed', 'key_blocks_unused', 'key_blocks_used', 'key_read_requests_persecond', 'key_reads_persecond', 'key_write_requests_persecond', 'key_writes_persecond', 'innodb_buffer_pool_instances', 'innodb_buffer_pool_size', 'innodb_flush_log_at_trx_commit', 'innodb_force_recovery', 'innodb_io_capacity', 'innodb_read_io_threads', 'innodb_write_io_threads', 'innodb_buffer_pool_pages_total', 'innodb_buffer_pool_pages_data', 'innodb_buffer_pool_pages_dirty', 'innodb_buffer_pool_pages_flushed', 'innodb_buffer_pool_pages_free', 'innodb_buffer_pool_pages_misc', 'innodb_page_size', 'innodb_pages_created', 'innodb_pages_read', 'innodb_pages_written', 'innodb_buffer_pool_pages_flushed_persecond', 'innodb_buffer_pool_read_requests_persecond', 'innodb_buffer_pool_reads_persecond', 'innodb_buffer_pool_write_requests_persecond', 'innodb_rows_read_persecond', 'innodb_rows_inserted_persecond', 'innodb_rows_updated_persecond', 'innodb_rows_deleted_persecond'], 'integer'],
            [['host', 'port'], 'required'],
            [['create_time'], 'safe'],
            [['host', 'role', 'innodb_version', 'innodb_flush_method'], 'string', 'max' => 30],
            [['port', 'innodb_doublewrite', 'innodb_file_per_table', 'query_cache_hitrate', 'thread_cache_hitrate', 'key_buffer_read_rate', 'key_buffer_write_rate', 'key_blocks_used_rate', 'created_tmp_disk_tables_rate', 'connections_usage_rate', 'open_files_usage_rate', 'open_tables_usage_rate'], 'string', 'max' => 10],
            [['tags', 'version'], 'string', 'max' => 50],
            [['innodb_row_lock_current_waits'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => '主机',
            'port' => '端口',
            'tags' => '标签',
            'connect' => '连接',
            'role' => '角色',
            'uptime' => '运行',
            'version' => '版本',
            'max_connections' => 'Max Connections',
            'max_connect_errors' => 'Max Connect Errors',
            'open_files_limit' => 'Open Files Limit',
            'open_files' => 'Open Files',
            'table_open_cache' => 'Table Open Cache',
            'open_tables' => 'Open Tables',
            'max_tmp_tables' => 'Max Tmp Tables',
            'max_heap_table_size' => 'Max Heap Table Size',
            'max_allowed_packet' => 'Max Allowed Packet',
            'threads_connected' => '连接线程',
            'threads_running' => '活动线程',
            'threads_waits' => '等待线程',
            'threads_created' => 'Threads Created',
            'threads_cached' => 'Threads Cached',
            'connections' => 'Connections',
            'aborted_clients' => 'Aborted Clients',
            'aborted_connects' => 'Aborted Connects',
            'connections_persecond' => 'Connections Persecond',
            'bytes_received_persecond' => 'Bytes Received Persecond',
            'bytes_sent_persecond' => 'Bytes Sent Persecond',
            'com_select_persecond' => 'Com Select Persecond',
            'com_insert_persecond' => 'Com Insert Persecond',
            'com_update_persecond' => 'Com Update Persecond',
            'com_delete_persecond' => 'Com Delete Persecond',
            'com_commit_persecond' => 'Com Commit Persecond',
            'com_rollback_persecond' => 'Com Rollback Persecond',
            'questions_persecond' => 'Questions Persecond',
            'queries_persecond' => '查询/秒',
            'transaction_persecond' => '事物/秒',
            'created_tmp_tables_persecond' => 'Created Tmp Tables Persecond',
            'created_tmp_disk_tables_persecond' => 'Created Tmp Disk Tables Persecond',
            'created_tmp_files_persecond' => 'Created Tmp Files Persecond',
            'table_locks_immediate_persecond' => 'Table Locks Immediate Persecond',
            'table_locks_waited_persecond' => 'Table Locks Waited Persecond',
            'key_buffer_size' => 'Key Buffer Size',
            'sort_buffer_size' => 'Sort Buffer Size',
            'join_buffer_size' => 'Join Buffer Size',
            'key_blocks_not_flushed' => 'Key Blocks Not Flushed',
            'key_blocks_unused' => 'Key Blocks Unused',
            'key_blocks_used' => 'Key Blocks Used',
            'key_read_requests_persecond' => 'Key Read Requests Persecond',
            'key_reads_persecond' => 'Key Reads Persecond',
            'key_write_requests_persecond' => 'Key Write Requests Persecond',
            'key_writes_persecond' => 'Key Writes Persecond',
            'innodb_version' => 'Innodb Version',
            'innodb_buffer_pool_instances' => 'Innodb Buffer Pool Instances',
            'innodb_buffer_pool_size' => 'Innodb Buffer Pool Size',
            'innodb_doublewrite' => 'Innodb Doublewrite',
            'innodb_file_per_table' => 'Innodb File Per Table',
            'innodb_flush_log_at_trx_commit' => 'Innodb Flush Log At Trx Commit',
            'innodb_flush_method' => 'Innodb Flush Method',
            'innodb_force_recovery' => 'Innodb Force Recovery',
            'innodb_io_capacity' => 'Innodb Io Capacity',
            'innodb_read_io_threads' => 'Innodb Read Io Threads',
            'innodb_write_io_threads' => 'Innodb Write Io Threads',
            'innodb_buffer_pool_pages_total' => 'Innodb Buffer Pool Pages Total',
            'innodb_buffer_pool_pages_data' => 'Innodb Buffer Pool Pages Data',
            'innodb_buffer_pool_pages_dirty' => 'Innodb Buffer Pool Pages Dirty',
            'innodb_buffer_pool_pages_flushed' => 'Innodb Buffer Pool Pages Flushed',
            'innodb_buffer_pool_pages_free' => 'Innodb Buffer Pool Pages Free',
            'innodb_buffer_pool_pages_misc' => 'Innodb Buffer Pool Pages Misc',
            'innodb_page_size' => 'Innodb Page Size',
            'innodb_pages_created' => 'Innodb Pages Created',
            'innodb_pages_read' => 'Innodb Pages Read',
            'innodb_pages_written' => 'Innodb Pages Written',
            'innodb_row_lock_current_waits' => 'Innodb Row Lock Current Waits',
            'innodb_buffer_pool_pages_flushed_persecond' => 'Innodb Buffer Pool Pages Flushed Persecond',
            'innodb_buffer_pool_read_requests_persecond' => 'Innodb Buffer Pool Read Requests Persecond',
            'innodb_buffer_pool_reads_persecond' => 'Innodb Buffer Pool Reads Persecond',
            'innodb_buffer_pool_write_requests_persecond' => 'Innodb Buffer Pool Write Requests Persecond',
            'innodb_rows_read_persecond' => 'Innodb Rows Read Persecond',
            'innodb_rows_inserted_persecond' => 'Innodb Rows Inserted Persecond',
            'innodb_rows_updated_persecond' => 'Innodb Rows Updated Persecond',
            'innodb_rows_deleted_persecond' => 'Innodb Rows Deleted Persecond',
            'query_cache_hitrate' => 'Query Cache Hitrate',
            'thread_cache_hitrate' => 'Thread Cache Hitrate',
            'key_buffer_read_rate' => 'Key Buffer Read Rate',
            'key_buffer_write_rate' => 'Key Buffer Write Rate',
            'key_blocks_used_rate' => 'Key Blocks Used Rate',
            'created_tmp_disk_tables_rate' => 'Created Tmp Disk Tables Rate',
            'connections_usage_rate' => 'Connections Usage Rate',
            'open_files_usage_rate' => 'Open Files Usage Rate',
            'open_tables_usage_rate' => 'Open Tables Usage Rate',
            'create_time' => 'Create Time',
        ];
    }

    /**
     * Before save.
     * 
     */
    /*public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            // add your code here
            return true;
        }
        else
            return false;
    }*/

    /**
     * After save.
     *
     */
    /*public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add your code here
    }*/

}
