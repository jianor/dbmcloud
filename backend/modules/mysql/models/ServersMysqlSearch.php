<?php

namespace backend\modules\mysql\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\mysql\models\ServersMysql;

/**
 * ServersMysqlSearch represents the model behind the search form about `backend\modules\mysql\models\ServersMysql`.
 */
class ServersMysqlSearch extends ServersMysql
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitor', 'send_mail', 'send_sms', 'alarm_threads_connected', 'alarm_threads_running', 'alarm_threads_waits', 'alarm_repl_status', 'alarm_repl_delay', 'threshold_warning_threads_connected', 'threshold_warning_threads_running', 'threshold_warning_threads_waits', 'threshold_warning_repl_delay', 'threshold_critical_threads_connected', 'threshold_critical_threads_running', 'threshold_critical_threads_waits', 'threshold_critical_repl_delay', 'slow_query', 'binlog_auto_purge', 'binlog_store_days', 'bigtable_monitor', 'bigtable_size', 'is_delete', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['host', 'port', 'username', 'password', 'tags', 'send_mail_to_list', 'send_sms_to_list', 'send_slowquery_to_list'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServersMysql::find();
        
        $query->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->load($params) && !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'monitor' => $this->monitor,
            'send_mail' => $this->send_mail,
            'send_sms' => $this->send_sms,
            'alarm_threads_connected' => $this->alarm_threads_connected,
            'alarm_threads_running' => $this->alarm_threads_running,
            'alarm_threads_waits' => $this->alarm_threads_waits,
            'alarm_repl_status' => $this->alarm_repl_status,
            'alarm_repl_delay' => $this->alarm_repl_delay,
            'threshold_warning_threads_connected' => $this->threshold_warning_threads_connected,
            'threshold_warning_threads_running' => $this->threshold_warning_threads_running,
            'threshold_warning_threads_waits' => $this->threshold_warning_threads_waits,
            'threshold_warning_repl_delay' => $this->threshold_warning_repl_delay,
            'threshold_critical_threads_connected' => $this->threshold_critical_threads_connected,
            'threshold_critical_threads_running' => $this->threshold_critical_threads_running,
            'threshold_critical_threads_waits' => $this->threshold_critical_threads_waits,
            'threshold_critical_repl_delay' => $this->threshold_critical_repl_delay,
            'slow_query' => $this->slow_query,
            'binlog_auto_purge' => $this->binlog_auto_purge,
            'binlog_store_days' => $this->binlog_store_days,
            'bigtable_monitor' => $this->bigtable_monitor,
            'bigtable_size' => $this->bigtable_size,
            'is_delete' => $this->is_delete,
            //'user_id' => $this->user_id,
            'user_id' => Yii::$app->user->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'port', $this->port])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'send_mail_to_list', $this->send_mail_to_list])
            ->andFilterWhere(['like', 'send_sms_to_list', $this->send_sms_to_list])
            ->andFilterWhere(['like', 'send_slowquery_to_list', $this->send_slowquery_to_list]);

        return $dataProvider;
    }
}
