<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\hbase\models\HbaseRsStatus */

$this->title = 'Create Hbase Rs Status';
$this->params['breadcrumbs'][] = ['label' => 'Hbase Rs Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hbase-rs-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
