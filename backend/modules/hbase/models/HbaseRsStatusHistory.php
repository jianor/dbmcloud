<?php

namespace backend\modules\hbase\models;

use Yii;

/**
 * This is the model class for table "hbase_rs_status_history".
 *
 * @property integer $id
 * @property string $host
 * @property integer $port
 * @property string $tags
 * @property integer $connect
 * @property integer $readRequestCount_sub
 * @property integer $writeRequestCount_sub
 * @property integer $compactionQueueLength
 * @property integer $flushQueueLength
 * @property integer $regionCount
 * @property integer $storeCount
 * @property integer $storeFileCount
 * @property integer $totalRequestCount_sub
 * @property integer $updatesBlockedTime
 * @property string $totalRequestCount
 * @property integer $GcCount
 * @property integer $GcTimeMillis
 * @property integer $GcTimeMillisConcurrentMarkSweep
 * @property integer $GcCountConcurrentMarkSweep
 * @property integer $queueSize
 * @property integer $QueueCallTime_num_ops_sub
 * @property integer $numCallsInGeneralQueue
 * @property string $memStoreSize
 * @property string $blockCacheSize
 * @property integer $MemHeapUsedM
 * @property string $create_time
 * @property string $readRequestCount
 * @property string $writeRequestCount
 * @property string $YmdHi
 */
class HbaseRsStatusHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hbase_rs_status_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['port', 'connect', 'readRequestCount_sub', 'writeRequestCount_sub', 'compactionQueueLength', 'flushQueueLength', 'regionCount', 'storeCount', 'storeFileCount', 'totalRequestCount_sub', 'updatesBlockedTime', 'totalRequestCount', 'GcCount', 'GcTimeMillis', 'GcTimeMillisConcurrentMarkSweep', 'GcCountConcurrentMarkSweep', 'queueSize', 'QueueCallTime_num_ops_sub', 'numCallsInGeneralQueue', 'memStoreSize', 'blockCacheSize', 'MemHeapUsedM', 'readRequestCount', 'writeRequestCount', 'YmdHi'], 'integer'],
            [['create_time'], 'required'],
            [['create_time'], 'safe'],
            [['host', 'tags'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'host' => 'Host',
            'port' => 'Port',
            'tags' => 'Tags',
            'connect' => 'Connect',
            'readRequestCount_sub' => 'Read Request Count Sub',
            'writeRequestCount_sub' => 'Write Request Count Sub',
            'compactionQueueLength' => 'Compaction Queue Length',
            'flushQueueLength' => 'Flush Queue Length',
            'regionCount' => 'Region Count',
            'storeCount' => 'Store Count',
            'storeFileCount' => 'Store File Count',
            'totalRequestCount_sub' => 'Total Request Count Sub',
            'updatesBlockedTime' => 'Updates Blocked Time',
            'totalRequestCount' => 'Total Request Count',
            'GcCount' => 'Gc Count',
            'GcTimeMillis' => 'Gc Time Millis',
            'GcTimeMillisConcurrentMarkSweep' => 'Gc Time Millis Concurrent Mark Sweep',
            'GcCountConcurrentMarkSweep' => 'Gc Count Concurrent Mark Sweep',
            'queueSize' => 'Queue Size',
            'QueueCallTime_num_ops_sub' => 'Queue Call Time Num Ops Sub',
            'numCallsInGeneralQueue' => 'Num Calls In General Queue',
            'memStoreSize' => 'Mem Store Size',
            'blockCacheSize' => 'Block Cache Size',
            'MemHeapUsedM' => 'Mem Heap Used M',
            'create_time' => 'Create Time',
            'readRequestCount' => 'Read Request Count',
            'writeRequestCount' => 'Write Request Count',
            'YmdHi' => 'Ymd Hi',
        ];
    }
}
