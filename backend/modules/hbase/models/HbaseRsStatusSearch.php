<?php

namespace backend\modules\hbase\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\hbase\models\HbaseRsStatus;

/**
 * HbaseRsStatusSearch represents the model behind the search form about `backend\modules\hbase\models\HbaseRsStatus`.
 */
class HbaseRsStatusSearch extends HbaseRsStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'port', 'connect', 'readRequestCount_sub', 'writeRequestCount_sub', 'compactionQueueLength', 'flushQueueLength', 'regionCount', 'storeCount', 'storeFileCount', 'totalRequestCount_sub', 'updatesBlockedTime', 'totalRequestCount', 'GcCount', 'GcTimeMillis', 'GcTimeMillisConcurrentMarkSweep', 'GcCountConcurrentMarkSweep', 'queueSize', 'QueueCallTime_num_ops_sub', 'numCallsInGeneralQueue', 'memStoreSize', 'blockCacheSize', 'MemHeapUsedM', 'readRequestCount', 'writeRequestCount'], 'integer'],
            [['host', 'tags', 'createtime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HbaseRsStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'port' => $this->port,
            'connect' => $this->connect,
            'readRequestCount_sub' => $this->readRequestCount_sub,
            'writeRequestCount_sub' => $this->writeRequestCount_sub,
            'compactionQueueLength' => $this->compactionQueueLength,
            'flushQueueLength' => $this->flushQueueLength,
            'regionCount' => $this->regionCount,
            'storeCount' => $this->storeCount,
            'storeFileCount' => $this->storeFileCount,
            'totalRequestCount_sub' => $this->totalRequestCount_sub,
            'updatesBlockedTime' => $this->updatesBlockedTime,
            'totalRequestCount' => $this->totalRequestCount,
            'GcCount' => $this->GcCount,
            'GcTimeMillis' => $this->GcTimeMillis,
            'GcTimeMillisConcurrentMarkSweep' => $this->GcTimeMillisConcurrentMarkSweep,
            'GcCountConcurrentMarkSweep' => $this->GcCountConcurrentMarkSweep,
            'queueSize' => $this->queueSize,
            'QueueCallTime_num_ops_sub' => $this->QueueCallTime_num_ops_sub,
            'numCallsInGeneralQueue' => $this->numCallsInGeneralQueue,
            'memStoreSize' => $this->memStoreSize,
            'blockCacheSize' => $this->blockCacheSize,
            'MemHeapUsedM' => $this->MemHeapUsedM,
            'createtime' => $this->createtime,
            'readRequestCount' => $this->readRequestCount,
            'writeRequestCount' => $this->writeRequestCount,
        ]);

        $query->andFilterWhere(['like', 'host', $this->host])
            ->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }
}
